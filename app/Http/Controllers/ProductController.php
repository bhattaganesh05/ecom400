<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\CustomTrait;
class ProductController extends Controller
{
    use CustomTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $product,$brand,$user,$category,$productImage;
    public function __construct(Product $product,Brand $brand,User $user,Category $category,ProductImage $productImage)
    {
        $this->product = $product;
        $this->brand = $brand;
        $this->user = $user;
        $this->category = $category;
        $this->productImage = $productImage;
    }
    private function validateId($id){
        $this->product = $this->product->find($id);
        if(!$this->product){
            request()->session()->flash('error','Sorry!, product does not exists');
            return  redirect()->route('product.index');
        }
    }
    public function index()
    {
        $all_products = $this->product->get();
        return view('admin.product.index')->with('product_list',$all_products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_cats = $this->category->getAllParentCats();
        if($all_cats){
            $all_cats = $all_cats->pluck('title','id');
        }
        $all_brand = $this->brand->where('status','active')->orderBy('title','ASC')->get();
        if($all_brand){
            $all_brand = $all_brand->pluck('title','id');
        }
        $all_seller = $this->user->getUserByRole('seller');
        if($all_seller){
            $all_seller = $all_seller->pluck('name','id');
        }
        return view('admin.product.form')
            ->with('category_list',$all_cats)
            ->with('brand_list',$all_brand)
            ->with('seller_list',$all_seller);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $request->validate($this->product->getRules());
        $data = $request->except('image');
        $data['slug'] = $this->getSlug($request->title,$this->product);
        $data['added_by'] = auth()->user()->id;
        $price = $data['price'];
        $data['actual_cost'] = $price - ($data['discount']*$price)/100;
        $this->product->fill($data);
        $status = $this->product->save();
        if($status){
            foreach ($request->image as $value) {
                $image_name = imageUpload($value,'product',env('ProductImageSize','500x500'));
                if($image_name){
                    $productImage = new ProductImage();
                    $productImage->product_id = $this->product->id;
                    $productImage->image = $image_name;
                    $productImage->save();
                }
            }
            $request->session()->flash('success','product successfully added.');
            return redirect()->route('product.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding product');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        $all_cats = $this->category->getAllParentCats();
        if($all_cats){
            $all_cats = $all_cats->pluck('title','id');
        }
        $all_brand = $this->brand->where('status','active')->orderBy('title','ASC')->get();
        if($all_brand){
            $all_brand = $all_brand->pluck('title','id');
        }
        $all_seller = $this->user->getUserByRole('seller');
        if($all_seller){
            $all_seller = $all_seller->pluck('name','id');
        }
        return  view('admin.product.form')
            ->with('category_list',$all_cats)
            ->with('brand_list',$all_brand)
            ->with('seller_list',$all_seller)
            ->with('product_detail',$this->product);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
//        dd($request->all());
        $request->validate($this->product->getRules('update'));
        $data = $request->except('image');
        $price = $request->price;
        $data['actual_cost'] = $price - ($data['discount']*$price)/100;
        $data['is_featured'] = $request->input('is_featured',0);
        $this->product->fill($data);
        $status = $this->product->save();
        if($status){
            if($request->image){
                foreach ($request->image as $value) {
                    $image_name = imageUpload($value,'product',env('ProductImageSize','500x500'));
                    if($image_name){
                        $productImage = new ProductImage();
                        $productImage->product_id = $this->product->id;
                        $productImage->image = $image_name;
                        $productImage->save();
                    }
                }
            }
            $del_images = $request->del_image;
            if($del_images != null){
                foreach ($del_images as $image_to_delete){
                    $this->productImage->deleteImageByName($image_to_delete);
                }
            }
            $request->session()->flash('success','product successfully updated.');
            return redirect()->route('product.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding updating');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $images = $this->product->images;
        $status = $this->product->delete();
        if($status){
            if($images->count() > 0){
                foreach ($images as $image) {
                    deleteImage($image->image, 'product');
                }
            }
            request()->session()->flash('success','product successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, product does not exists');
        }
        return redirect()->route('product.index');
    }
    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->product->status == 'active') {
            $this->product->status = 'inactive';
        }else{
            $this->product->status = 'active';
        }
        $status = $this->product->save();
        if($status){
            $request->session()->flash('success','product status successfully changed ');
            return redirect()->route('product.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing product status');
            return redirect()->back();
        }
    }
/*for seller*/
    public function validateIdForSeller($id){
        $this->product = $this->product->find($id);
        if(!$this->product){
            request()->session()->flash('error','Sorry!, product does not exists');
            return  redirect()-route('seller.product');
        }
        if($this->product->seller_id !== request()->user()->id){
            request()->session()->flash('error','Sorry!, product does not belongs to you.');
            return  redirect()-route('seller.product');
        }
    }
    public function getProductBySeller()
    {
        $all_products = $this->product->getProductBySeller(request()->user()->id);
        return view('seller.product.index')->with('product_list',$all_products);
    }
    public function showSellerProductForm(Request $request){
        $all_cats = $this->category->getAllParentCats();
        if($all_cats){
            $all_cats = $all_cats->pluck('title','id');
        }
        $all_brand = $this->brand->where('status','active')->orderBy('title','ASC')->get();
        if($all_brand){
            $all_brand = $all_brand->pluck('title','id');
        }
        return view('seller.product.form')
            ->with('category_list',$all_cats)
            ->with('brand_list',$all_brand);
    }
    public function storeProduct(Request $request){
        $rules = $this->product->getRules();
        unset($rules['seller_id']);
        unset($rules['status']);
        $request->validate($rules);
        $data = $request->except('image');
        $data['slug'] = $this->getSlug($request->title,$this->product);
        $data['added_by'] = auth()->user()->id;
        $price = $data['price'];
        $data['actual_cost'] = $price - ($data['discount']*$price)/100;
        $data['seller_id'] = $request->user()->id;
        $data['status'] = 'inactive';
        $this->product->fill($data);
        $status = $this->product->save();
        if($status){
            foreach ($request->image as $value) {
                $image_name = imageUpload($value,'product',env('ProductImageSize','500x500'));
                if($image_name){
                    $productImage = new ProductImage();
                    $productImage->product_id = $this->product->id;
                    $productImage->image = $image_name;
                    $productImage->save();
                }
            }
            $request->session()->flash('success','product successfully added.');
            return redirect()->route('seller.product');
        }else{
            $request->session()->flash('error','Sorry!, error while adding product');
            return redirect()->back();
        }
    }
    public function getProductEditForm($id){
        $this->validateIdForSeller($id);
        $all_cats = $this->category->getAllParentCats();
        if($all_cats){
            $all_cats = $all_cats->pluck('title','id');
        }
        $all_brand = $this->brand->where('status','active')->orderBy('title','ASC')->get();
        if($all_brand){
            $all_brand = $all_brand->pluck('title','id');
        }

        return  view('seller.product.form')
            ->with('category_list',$all_cats)
            ->with('brand_list',$all_brand)
            ->with('product_detail',$this->product);
    }
    public function updateProduct(Request $request,$id){
        $this->validateIdForSeller($id);
        $rules = $this->product->getRules();
        unset($rules['seller_id']);
        unset($rules['status']);
        $request->validate($rules);
//        dd($request->all());
        $data = $request->except('image');
        $price = $request->price;
        $data['actual_cost'] = $price - ($data['discount']*$price)/100;
        $data['is_featured'] = $request->input('is_featured',0);
        $this->product->fill($data);
        $status = $this->product->save();
        if($status){
            if($request->image){
                foreach ($request->image as $value) {
                    $image_name = imageUpload($value,'product',env('ProductImageSize','500x500'));
                    if($image_name){
                        $productImage = new ProductImage();
                        $productImage->product_id = $this->product->id;
                        $productImage->image = $image_name;
                        $productImage->save();
                    }
                }
            }
            $del_images = $request->del_image;
            if($del_images != null){
                foreach ($del_images as $image_to_delete){
                    $this->productImage->deleteImageByName($image_to_delete);
                }
            }
            $request->session()->flash('success','product successfully updated.');
            return redirect()->route('seller.product');
        }else{
            $request->session()->flash('error','Sorry!, error while adding updating');
            return redirect()->back();
        }
    }
    public function deleteProduct($id){
        $this->validateIdForSeller($id);
        $images = $this->product->images;
        $status = $this->product->delete();
        if($status){
            if($images->count() > 0){
                foreach ($images as $image) {
                    deleteImage($image->image, 'product');
                }
            }
            request()->session()->flash('success','product successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, product does not exists');
        }
        return redirect()->route('seller.product');
    }
}
