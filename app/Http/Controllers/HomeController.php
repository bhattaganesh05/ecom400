<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected  $user;
    protected  $user_info;

    public function __construct(User $user,UserInfo $user_info)
    {
        $this->user = $user;
        $this->user_info = $user_info;

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route(request()->user()->role);
    }
    public function admin(){
        if(request()->user()->role != "admin"){
            return redirect()->route(request()->user()->role);
        }
        return view('admin.dashboard');
    }
    public function customer(){
        if(request()->user()->role != "customer"){
            return redirect()->route(request()->user()->role);
        }
        return view('customer.dashboard');
    }
    public function seller(){
        if(request()->user()->role != "seller"){
            return redirect()->route(request()->user()->role);
        }
        return view('seller.dashboard');
    }
    public  function  updateMe(Request $request){
        $this->user = $request->user();
        $rules = $this->user->getUpdateRule();
        $request->validate($rules);
        $data = $request->except('image');
        if($request->image){
            $file_name = imageUpload($request->image,'user',env('UserImageSize','200x200'));
            if($file_name){
                if(!(request()->user()->userInfo->image)){
                    deleteImage(request()->user()->userInfo->image,'user');
                }
                $data['image'] = $file_name;
            }
        }
//                dd($data);
        $this->user->fill($data);
        $status = $this->user->save();
        if($status){
            $this->user_info = $this->user_info->where('user_id',$this->user->id)->first();
            if(!$this->user_info){
                $this->user_info = new UserInfo();
            }
            $data['added_by'] = auth()->user()->id;
            $data['user_id'] = auth()->user()->id;
            $this->user_info->fill($data);
            $this->user_info->save();
            $request->session()->flash('success','User updated successfully');
            return redirect()->back();
        }else{
            $request->session()->flash('error','Sorry!, error while updating this user.');
            return redirect()->back();
        }
        return redirect()->back();
    }

    public  function updatePassword(Request $request){
        $this->user = auth()->user();
        $request->validate($this->user->getUpdatePassword());
        $data = $request->all();
        if(Hash::check($data['current_password'], auth()->user()->password)){
            $this->user->password = bcrypt($data['password']);
                $status = $this->user->save();
                if($status){
                   Auth::logout();
                    $request->session()->flash('success','Password updated successfully. Please login to continue');
                    return redirect()->route('login');
                }else{
                    $request->session()->flash('error','Sorry!, error while updating password');
                    return redirect()->back();
                }
        }else{
            $request->session()->flash('error','Sorry!, your this current password does not match');
            return redirect()->back();
        }
    }
}
