<?php

namespace App\Http\Controllers;

use App\Models\OfferCollectionProduct;
use App\Models\Product;
use App\Models\OfferProductCollection;
use Illuminate\Http\Request;
use App\Traits\CustomTrait;

class OfferProductCollectionController extends Controller
{
    use CustomTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $product;
    protected $offer_product_collection;
    public function __construct(Product $product,OfferProductCollection $offer_product_collection)
    {
        $this->product = $product;
        $this->offer_product_collection = $offer_product_collection;
    }
    private function validateId($id){
        $this->offer_product_collection = $this->offer_product_collection->find($id);
        if(!$this->offer_product_collection){
            request()->session()->flash('error','Sorry!, Offer does not exists');
            return  redirect()-route('offer.index');
        }
    }
    public function index()
    {
        $offer_collection_list = $this->offer_product_collection->get();
        return view('admin.offer.index')->with('offer_collection_list',$offer_collection_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_list = $this->product->pluck('title','id');
        return  view('admin.offer.form')->with('product_list',$product_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->offer_product_collection->getRules());
        $data = $request->except(['image','product_id']);
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->getSlug($request->title,$this->offer_product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'offer',env('OfferImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->offer_product_collection->fill($data);
        $status = $this->offer_product_collection->save();
        if($status){
            if($request->product_id){
                foreach ($request->product_id as $product_id){
                    $collection_product = new OfferCollectionProduct();
                    $collection_product->collection_id = $this->offer_product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                    // for update in discount of products table
                    $product =  new Product();
                    $product = $product->find($product_id);
//                    dd($product->discount);
                    $product->discount = $request->discount;
                    $product->save();

                }
            }
            $request->session()->flash('success','Offer successfully created');
            return redirect()->route('offer.index');
        }else{
            $request->session()->flash('error','Sorry!, error while creating offer');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        $product_list = $this->product->pluck('title','id');
        return  view('admin.offer.form')
            ->with('offer_detail',$this->offer_product_collection)
            ->with('product_list',$product_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
        $request->validate($this->offer_product_collection->getRules('update'));
        $data = $request->except(['image','product_id']);
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->getSlug($request->title,$this->offer_product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'collection',env('CollectionImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
            if($this->offer_product_collection->image != null){
                deleteImage($this->offer_product_collection->image,'collection');
            }
        }
        $this->offer_product_collection->fill($data);
        $status = $this->offer_product_collection->save();
        if($status){
            if($request->product_id){
                OfferCollectionProduct::where('collection_id',$this->offer_product_collection->id)->delete();
                foreach ($request->product_id as $product_id){
                    $collection_product = new OfferCollectionProduct();
                    $collection_product->collection_id = $this->offer_product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                }
            }
            if($request->discount){
                if($request->product_id){
                    foreach ($request->product_id as $product_id){
                        $product =  new Product();
                        $product = $product->find($product_id);
                        $product->discount = $request->discount;
                        $product->save();
                    }
                }
            }
            $request->session()->flash('success','Collection of products successfully updated');
            return redirect()->route('offer.index');
        }else{
            $request->session()->flash('error','Sorry!, error while updating collection of products');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->offer_product_collection->image;
        $status = $this->offer_product_collection->delete();
        if($status){
            if($image != null){
                deleteImage($image,'collection');
            }
            request()->session()->flash('success','Offer successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Offer does not exists');
        }
        return redirect()->route('offer.index');
    }
    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->offer_product_collection->status == 'active') {
            $this->offer_product_collection->status = 'inactive';
        }else{
            $this->offer_product_collection->status = 'active';
        }
        $status = $this->offer_product_collection->save();
        if($status){
            $request->session()->flash('success','Offer status successfully changed ');
            return redirect()->route('offer.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing offer status');
            return redirect()->back();
        }
    }
}
