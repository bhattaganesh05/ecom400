<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }


    private function validateId($id){
        $this->page = $this->page->find($id);
        if(!$this->page){
            request()->session()->flash('error','Sorry!, page does not exists');
            return  redirect()-route('page.index');
        }
    }

    public function index()
    {
        return view('admin.page.index')->with('page_list',$this->page->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        return  view('admin.page.form')->with('page_detail',$this->page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
        $request->validate([
            'summary' => ["required","string"],
            'description' => ["nullable","string"],
            'status' => ["required","in:active,inactive"],
            'image' => ["sometimes","image","max:5000"]
        ]);
        $data = $request->except('image');
        if($request->image){
            $image_name = imageUpload($request->image,'page',env('PageBannerSize','200x200'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $data['updated_by'] =  $request->user()->id;
        $this->page->fill($data);
        $status = $this->page->save();
        if($status){
            $request->session()->flash('success', 'page  updated successfully.');
        }else{
            $request->session()->flash('error','Sorry!, error while updating page.');
        }
        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->page->status == 'active') {
            $this->page->status = 'inactive';
        }else{
            $this->page->status = 'active';
        }
        $status = $this->page->save();
        if($status){
            $request->session()->flash('success', 'page status successfully changed ');
        }else{
            $request->session()->flash('error','Sorry!, error while changing page status');
        }
        return redirect()->route('page.index');
    }
}
