<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $blog;
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }
    private function validateId($id){
        $this->blog = $this->blog->find($id);
        if(!$this->blog){
            request()->session()->flash('error','Sorry!, blog does not exists');
            return  redirect()->route('blog.index');
        }
    }
    public function index()
    {
        $blog_list = $this->blog->get();
        return  view('admin.blog.index')->with('blog_list',$blog_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.blog.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->blog->getRules());
        $data = $request->except('image');
        $data['slug'] = \Str::slug($request->title);
        $data['added_by'] = auth()->user()->id;
            $image_name = imageUpload($request->image,'blog',env('BlogBannerSize','1280x768'));
            if($image_name){
                $data['image'] = $image_name;
            }
        $this->blog->fill($data);
        $status = $this->blog->save();
        if($status){
            $request->session()->flash('success','blog successfully added.');
            return redirect()->route('blog.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding blog');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        return  view('admin.blog.form')
            ->with('blog_detail',$this->blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
//        dd($request->all());
        $request->validate($this->blog->getRules('update'));
        $data = $request->except('image');
        if($request->image){
            $image_image = imageUpload($request->image,'blog',env('BlogBannerSize','1280x768'));
            if($image_image){
                $data['image'] = $image_image;
            }
            if($this->blog->image){
                deleteImage($this->blog->image,'blog');
            }
        }
        $this->blog->fill($data);
        $status = $this->blog->save();
        if($status){
            $request->session()->flash('success','blog successfully updated.');
            return redirect()->route('blog.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding updating');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->blog->image;
        $status = $this->blog->delete();
        if($status){
            if($image != null){
                deleteImage($image,'blog');
            }
            request()->session()->flash('success','Blog successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Blog does not exists');
        }
        return redirect()->route('blog.index');
    }
    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->blog->status == 'active') {
            $this->blog->status = 'inactive';
        }else{
            $this->blog->status = 'active';
        }
        $status = $this->blog->save();
        if($status){
            $request->session()->flash('success','blog status successfully changed ');
            return redirect()->route('blog.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing blog status');
            return redirect()->back();
        }
    }
}
