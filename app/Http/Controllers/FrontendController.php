<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Blog;
use App\Models\Page;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCollection;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    protected $product,$page,$blog,$banner,$category,$collection;
    public function __construct(Product $product,Page $page,Blog $blog,Banner $banner,Category $category,ProductCollection $collection)
    {
        $this->product = $product;
        $this->page = $page;
        $this->blog = $blog;
        $this->banner = $banner;
        $this->category = $category;
        $this->collection = $collection;
    }

    public function homePage(){
        return view('front.home')
            ->with('parent_cats',$this->category->getAllCategories())
            ->with('banner_data',$this->banner->where('status','active')->orderBy('id','DESC')->first())
            ->with('featured_products',$this->product->getFeaturedProducts())
            ->with('latest_products',$this->product->getProducts(6))
            ->with('top_rated_products',$this->product->getTopRatedProducts())
            ->with('top_blogs',$this->blog->getBlog(3))
            ->with('latest_collections',$this->collection->getLatestCollection());
    }

    public function listAllProducts(Request $request){
        return view('front.product.list-all')
            ->with('product_list',$this->product->getProducts())
            ->with('latest_products',$this->product->getProducts(6));
    }
    public function getPageDetail(Request $request){
        $this->page = $this->page->where('slug',$request->slug)->firstOrFail();
        return view('front.page.detail')->with('page_detail',$this->page);
    }

    public function getAllBlog(Request $request){
        $this->blog = $this->blog->where('status','active')->orderBy('id','DESC')->get();
        return view('front.blog.list-all')->with('all_blogs',$this->blog);
    }
    public function getContactUs(Request $request){
        return view('front.page.contact-us');
    }
    public function searchResult(Request $request){
        $query = $this->product;
        if(isset($request->keyword) && !empty($request->keyword)){
            $query = $query->where('title','LIKE',"%".$request->keyword."%")
                ->orWhere('summary','LIKE',"%".$request->keyword."%")
                ->orWhere('description','LIKE',"%".$request->keyword."%");
        }
        if(isset($request->min) && $request->min > 50){
            $min_range = (int)$request->min;
            $query = $query->where('actual_cost','>=', $min_range);
        }
        if(isset($request->max) && $request->max > 0){
            $max_range = (int)$request->max;
            $query = $query->where('actual_cost','<=', $max_range);
        }
        $all_prod = $query->where('status','active')->orderBy('id','DESC')->paginate(12);
        return view('front.product.list-all')
            ->with('product_list',$all_prod)
            ->with('latest_products',$this->product->getProducts(6));
    }

    public function getProductFromSubCategory(Request $request){
        $sub_category = $this->category->where('status','active')->where('slug',$request->slug)-> firstOrFail();
        $all_prod = $this->product->where('status','active')->where('sub_category_id',$sub_category->id)->orderBy('id','DESC')->paginate(12);
        return view('front.product.list-all')
            ->with('product_list',$all_prod)
            ->with('latest_products',$this->product->getLatestProductsBySubCategoryId($sub_category->id));
    }
    public function getProductFromCategory(Request $request){
        $this->category = $this->category->where('status','active')->where('slug',$request->slug)-> firstOrFail();
        $all_prod = $this->product->where('status','active')->where('category_id',$this->category->id)->orderBy('id','DESC')->paginate(12);
        return view('front.product.list-all')
            ->with('product_list',$all_prod)
            ->with('latest_products',$this->product->getLatestProductsByCategoryId($this->category->id));
    }
    public function getProductDetail(Request $request){
        $product = $this->product->where('slug',$request->slug)->firstOrFail();
        $all_reviews = $product->productReviews;
        if($all_reviews->count() <= 0 ){
            $avg_review = 0;
        }else{
            $avg_review = ceil($all_reviews->sum('rate')/$all_reviews->count());
        }
        if($avg_review > 5){
            $avg_review = 5;
        }
        return view('front.product.detail')
            ->with('avg_rate',$avg_review)
            ->with('product_detail',$product);
    }


}
