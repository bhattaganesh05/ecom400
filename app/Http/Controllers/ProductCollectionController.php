<?php

namespace App\Http\Controllers;

use App\Models\CollectionProduct;
use App\Models\Product;
use App\Models\ProductCollection;
use Illuminate\Http\Request;
use App\Traits\CustomTrait;

class ProductCollectionController extends Controller
{
    use CustomTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $product;
    protected $product_collection;
    public function __construct(Product $product,ProductCollection $product_collection)
    {
        $this->product = $product;
        $this->product_collection = $product_collection;
    }
    private function validateId($id){
        $this->product_collection = $this->product_collection->find($id);
        if(!$this->product_collection){
            request()->session()->flash('error','Sorry!, collection does not exists');
            return  redirect()-route('collection.index');
        }
    }
    public function index()
    {
        $collection_list = $this->product_collection->get();
        return view('admin.collection.index')->with('collection_list',$collection_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_list = $this->product->pluck('title','id');
        return  view('admin.collection.form')->with('product_list',$product_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->product_collection->getRules());
        $data = $request->except(['image','product_id']);
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->getSlug($request->title,$this->product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'collection',env('CollectionImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->product_collection->fill($data);
        $status = $this->product_collection->save();
        if($status){
            if($request->product_id){
                foreach ($request->product_id as $product_id){
                    $collection_product = new CollectionProduct();
                    $collection_product->collection_id = $this->product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                }
            }
            $request->session()->flash('success','Collection of products successfully created');
            return redirect()->route('collection.index');
        }else{
            $request->session()->flash('error','Sorry!, error while creating collection of products');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        $product_list = $this->product->pluck('title','id');
        return  view('admin.collection.form')
            ->with('collection_detail',$this->product_collection)
            ->with('product_list',$product_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
        $request->validate($this->product_collection->getRules('update'));
        $data = $request->except(['image','product_id']);
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->getSlug($request->title,$this->product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'collection',env('CollectionImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
            if($this->product_collection->image != null){
                deleteImage($this->product_collection->image,'collection');
            }
        }
        $this->product_collection->fill($data);
        $status = $this->product_collection->save();
        if($status){
            if($request->product_id){
                CollectionProduct::where('collection_id',$this->product_collection->id)->delete();
                foreach ($request->product_id as $product_id){
                    $collection_product = new CollectionProduct();
                    $collection_product->collection_id = $this->product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                }
            }
            $request->session()->flash('success','Collection of products successfully updated');
            return redirect()->route('collection.index');
        }else{
            $request->session()->flash('error','Sorry!, error while updating collection of products');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->product_collection->image;
        $status = $this->product_collection->delete();
        if($status){
            if($image != null){
                deleteImage($image,'collection');
            }
            request()->session()->flash('success','Collection successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Collection does not exists');
        }
        return redirect()->route('collection.index');
    }
    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->product_collection->status == 'active') {
            $this->product_collection->status = 'inactive';
        }else{
            $this->product_collection->status = 'active';
        }
        $status = $this->product_collection->save();
        if($status){
            $request->session()->flash('success','collection status successfully changed ');
            return redirect()->route('collection.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing collection status');
            return redirect()->back();
        }
    }


    /*for seller*/
    public function validateIdForSeller($id){
        $this->product_collection = $this->product_collection->find($id);
        if(!$this->product_collection){
            request()->session()->flash('error','Sorry!, collection does not exists');
            return  redirect()-route('seller.collection');
        }
        if($this->product_collection->added_by != \request()->user()->id){
            request()->session()->flash('error','Sorry!, collection does not belongs to you');
            return  redirect()-route('seller.collection');
        }
    }
    public function getCollectionBySeller()
    {
        return view('seller.collection.index')->with('collection_list',$this->product_collection->where('added_by',request()->user()->id)->get());
    }
    public function showSellerCollectionForm(Request $request){
        $product_list = $this->product->where('seller_id',$request->user()->id)->pluck('title','id');
        return  view('seller.collection.form')->with('product_list',$product_list);
    }
    public function storeCollection(Request $request){
        $request->request->add(['status'=>'inactive']);
        $rules = $this->product_collection->getRules();
//        unset($rules['status']);
        $request->validate($rules);
        $data = $request->except(['image','product_id']);
        $data['added_by'] = auth()->user()->id;
        $data['status'] = 'inactive';
        $data['slug'] = $this->getSlug($request->title,$this->product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'collection',env('CollectionImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->product_collection->fill($data);
        $status = $this->product_collection->save();
        if($status){
            if($request->product_id){
                foreach ($request->product_id as $product_id){
                    $collection_product = new CollectionProduct();
                    $collection_product->collection_id = $this->product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                }
            }
            $request->session()->flash('success','Collection successfully created');
            return redirect()->route('seller.collection');
        }else{
            $request->session()->flash('error','Sorry!, error while creating collection');
            return redirect()->back();
        }
    }
    public function getCollectionEditForm($id){
        $this->validateIdForSeller($id);
//        dd($this->product_collection);
        $product_list = $this->product->pluck('title','id');
        return  view('seller.collection.form')
            ->with('collection_detail',$this->product_collection)
            ->with('product_list',$product_list);
    }
    public function updateCollection(Request $request,$id){
        $this->validateIdForSeller($id);
        $rules = $this->product_collection->getRules('update');
        unset($rules['status']);
        $request->validate($rules);
        $data = $request->except(['image','product_id']);
        $data['status'] = 'inactive';
        $data['slug'] = $this->getSlug($request->title,$this->product_collection);
//        dd($data);
        if($request->image){
            $image_name = imageUpload($request->image,'collection',env('CollectionImageSize','200x150'));
            if($image_name){
                $data['image'] = $image_name;
            }
            if($this->product_collection->image != null){
                deleteImage($this->product_collection->image,'collection');
            }
        }
        $this->product_collection->fill($data);
        $status = $this->product_collection->save();
        if($status){
            if($request->product_id){
                CollectionProduct::where('collection_id',$this->product_collection->id)->delete();
                foreach ($request->product_id as $product_id){
                    $collection_product = new CollectionProduct();
                    $collection_product->collection_id = $this->product_collection->id;
                    $collection_product->product_id = $product_id;
                    $collection_product->save();

                }
            }
            $request->session()->flash('success','Collection successfully updated');
            return redirect()->route('seller.collection');
        }else{
            $request->session()->flash('error','Sorry!, error while updating collection.');
            return redirect()->back();
        }
    }
    public function deleteCollection($id){
        $this->validateIdForSeller($id);
        $image = $this->product_collection->image;
        $status = $this->product_collection->delete();
        if($status){
            if($image != null){
                deleteImage($image,'collection');
            }
            request()->session()->flash('success','Collection successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Collection does not exists');
        }
        return redirect()->route('seller.collection');
    }

}
