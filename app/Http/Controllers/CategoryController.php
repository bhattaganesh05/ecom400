<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category;
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $parent_cat_list = $this->category->getAllParentCats();
        return  view('admin.category.index')->with('parent_cat_list',$parent_cat_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_parents = $this->category->getAllParentCats();
        if($all_parents != NULL){
            $all_parents = $all_parents->pluck('title','id');
        }
        return  view('admin.category.form')->with('all_parents',$all_parents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->category->categoryRules());
        $data = $request->except('image');
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->category->getSlug($request->title);
        if($request->image){
            $image_name = imageUpload($request->image,'category',env('CategoryImageSize','500x500'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->category->fill($data);
//        dd($this->category);
        $status = $this->category->save();
        if($status){
            $request->session()->flash('success','Category successfully added.');
            return redirect()->route('category.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding category');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);
        $all_parents = $this->category->getAllParentCats();
        if($all_parents != NULL){
            $all_parents = $all_parents->pluck('title','id');
        }
        return  view('admin.category.form')->with('cats_detail',$this->category)->with('all_parents',$all_parents);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->category = $this->category->find($id);
        $request->validate($this->category->categoryRules($act = 'update'));
        $data = $request->except('image');
        if($request->image){
            $image_image = imageUpload($request->image,'category',env('CategoryImageSize','1280x768'));
            if($image_image){
                $data['image'] = $image_image;
            }
            if($this->category->image){
                deleteImage($this->category->image,'category');
            }
        }
        $this->category->fill($data);
        $status = $this->category->save();
        if($status){
            $request->session()->flash('success','Parent category successfully updated');
            return redirect()->route('category.index');
        }else{
            $request->session()->flash('error','Sorry!, error while updating parent category');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->category->image;
        $status = $this->category->delete();
        if($status){
            if($image != null){
                deleteImage($image,'category');
            }
            request()->session()->flash('success','Category successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Category does not exists');
        }
        return redirect()->route('category.index');
    }

    private function validateId($id){
        $this->category = $this->category->find($id);
        if(!$this->category){
            request()->session()->flash('error','Sorry!, Category does not exists');
            return  redirect()-route('category.index');
        }
    }

    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->category->status == 'active') {
            $this->category->status = 'inactive';
        }else{
            $this->category->status = 'active';
        }
        $status = $this->category->save();
        if($status){
            $request->session()->flash('success','Category status successfully changed ');
            return redirect()->route('category.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing category status');
            return redirect()->back();
        }
    }
    public function getAllSubCats(Request $request){
        $this->category = $this->category->find($request->cat_id);
        if(!$this->category){
            return response()->json([
                'status' => false,
                'message' => 'Category not found',
                'data' => NULL
            ]);
        }
//        $child_cats = $this->category->getChildCategories($this->category->id);
        $sub_cats = $this->category->getsubCategories;
//        dd($sub_cats);
        if($sub_cats->count()){
            return response()->json([
                'status' => true,
                'message' => 'success',
                'data' => $sub_cats
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'No category found in this parent',
                'data' => NULL
            ]);
        }

    }
}
