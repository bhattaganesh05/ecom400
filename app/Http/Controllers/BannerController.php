<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    protected $banner;
    public function  __construct(Banner $banner)
    {
        $this->banner = $banner;

    }

    public function index()
    {
        $banner_list = $this->banner->get();
        return view('admin.banner.index')->with('banner_list',$banner_list);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.banner.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->banner->bannerRules());
        $data = $request->except('image');
        $data['added_by'] = auth()->user()->id;
        if($request->image){
            $image_name = imageUpload($request->image,'banner',env('BannerImageSize','1280x768'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->banner->fill($data);
        $status = $this->banner->save();
        if($status){
            $request->session()->flash('success','Banner successfully added');
            return redirect()->route('banner.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding banner');
            return redirect()->back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    private function validateId($id){
        $this->banner = $this->banner->find($id);
        if(!$this->banner){
            request()->session()->flash('error','Sorry!, Banner does not exists');
            return  redirect()-route('banner.index');
        }
    }
    public function edit($id)
    {
        $this->validateId($id);
        return  view('admin.banner.form')->with('banner_detail',$this->banner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $this->banner = $banner;
        $request->validate($this->banner->bannerRules($act = 'update'));
        $data = $request->except('image');
        if($request->image){
            $image_image = imageUpload($request->image,'banner',env('BannerImageSize','1280x768'));
            if($image_image){
                $data['image'] = $image_image;
            }
            if($this->banner->image){
                deleteImage($this->banner->image,'banner');
            }
        }
        $this->banner->fill($data);
        $status = $this->banner->save();
        if($status){
            $request->session()->flash('success','Banner successfully updated');
            return redirect()->route('banner.index');
        }else{
            $request->session()->flash('error','Sorry!, error while updating banner');
            return redirect()->back();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->banner->image;
        $status = $this->banner->delete();
        if($status){
            if($image != null){
                deleteImage($image,'banner');
            }
            request()->session()->flash('success','Banner successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Banner does not exists');
        }
        return redirect()->route('banner.index');
    }
    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->banner->status == 'active') {
            $this->banner->status = 'inactive';
        }else{
            $this->banner->status = 'active';
        }
        $status = $this->banner->save();
        if($status){
            $request->session()->flash('success','Banner status successfully changed ');
            return redirect()->route('banner.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing banner status');
            return redirect()->back();
        }
    }
}
