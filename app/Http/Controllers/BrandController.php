<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $brand;
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function index()
    {
        $brand_list = $this->brand->get();
        return  view('admin.brand.index')->with('brand_list',$brand_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.brand.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->brand->brandRules());
        $data = $request->except('image');
        $data['added_by'] = auth()->user()->id;
        $data['slug'] = $this->brand->getSlug($request->title);
        if($request->image){
            $image_name = imageUpload($request->image,'brand',env('BrandImageSize','500x500'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }
        $this->brand->fill($data);
//        dd($this->brand);
        $status = $this->brand->save();
        if($status){
            $request->session()->flash('success','Brand successfully added.');
            return redirect()->route('brand.index');
        }else{
            $request->session()->flash('error','Sorry!, error while adding brand');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validateId($id);

        return  view('admin.brand.form')->with('brand_detail',$this->brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->brand = $this->brand->find($id);
        $request->validate($this->brand->brandRules($act = 'update'));
        $data = $request->except('image');
        if($request->image){
            $image_image = imageUpload($request->image,'brand',env('BrandImageSize','1280x768'));
            if($image_image){
                $data['image'] = $image_image;
            }
            if($this->brand->image){
                deleteImage($this->brand->image,'brand');
            }
        }
        $this->brand->fill($data);
        $status = $this->brand->save();
        if($status){
            $request->session()->flash('success','Parent brand successfully updated');
            return redirect()->route('brand.index');
        }else{
            $request->session()->flash('error','Sorry!, error while updating parent brand');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->validateId($id);
        $image = $this->brand->image;
        $status = $this->brand->delete();
        if($status){
            if($image != null){
                deleteImage($image,'brand');
            }
            request()->session()->flash('success','Brand successfully deleted');
        }else{
            request()->session()->flash('error','Sorry!, Brand does not exists');
        }
        return redirect()->route('brand.index');
    }

    private function validateId($id){
        $this->brand = $this->brand->find($id);
        if(!$this->brand){
            request()->session()->flash('error','Sorry!, Brand does not exists');
            return  redirect()-route('brand.index');
        }
    }

    public function changeStatus(Request $request){
        $this->validateId($request->id);
        if($this->brand->status == 'active') {
            $this->brand->status = 'inactive';
        }else{
            $this->brand->status = 'active';
        }
        $status = $this->brand->save();
        if($status){
            $request->session()->flash('success','Brand status successfully changed ');
            return redirect()->route('brand.index');
        }else{
            $request->session()->flash('error','Sorry!, error while changing brand status');
            return redirect()->back();
        }
    }
}
