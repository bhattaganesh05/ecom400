<?php
function imageUpload($file,$dir,$thumb = null){
    $upload_dir = public_path()."/uploads/".$dir;
    if(!File::exists($upload_dir)){
        File::makeDirectory($upload_dir,0777,true,true);
    }
    $file_name = ucfirst($dir)."-".date('YmdHis').rand(0,999).".".$file->getClientOriginalExtension();
    $status = $file->move($upload_dir,$file_name);
    if($status){
        if($thumb !== null){
            list($thumb_width,$thumb_height) = explode("x",$thumb);
            Image::make($upload_dir.'/'.$file_name)->resize($thumb_width,$thumb_height,function($constraint){
                return $constraint->aspectRatio();
            })->save($upload_dir.'/'.$file_name);
        }
        return $file_name;
    }else{
        return  false;
    }

}
function  deleteImage($image_name,$dir){
    $user_image_path = public_path('uploads/'.$dir);
    if(File::exists($user_image_path) && File::exists($user_image_path.'/'.$image_name)){
        unlink($user_image_path.'/'.$image_name);
        return true;
    }else{
        return false;
    }
}
function getCategoryMenu(){
    $category = new App\Models\Category();
    $category = $category->getAllCategories();
    if($category->count()){
        $category_html = '';
        foreach ($category as $parent_category){
            $sub_category = $parent_category->getsubCategories()->where('status','active')->get();
            if($sub_category->count()){
                $category_html .= '<li class="dropdown" style="">
                <a href="#" class="dropbtn">'.$parent_category->title.'</a>
                <div class="dropdown-content text-center" style="left:0; position: relative;">';
                foreach ($sub_category as $child_cat){
                    $category_html .= '<a href="'.route('sub-cat-products',[$parent_category->slug,$child_cat->slug]).'">'.$child_cat->title.'</a>';
                }
                $category_html .= '</div></li>';
            }else{
                $category_html .= '<li><a href="'.route('cat-products',$parent_category->slug).'" >'.$parent_category->title.'</a></li>';
            }
        }
    }
        return $category_html;
//   return ('<li class="dropdown" style="">
//                <a href="" class="dropbtn">Fresh Meat</a>
//                <div class="dropdown-content text-center" style="left:0; position: relative; ">
//                    <a href="#">Link 1</a>
//                    <a href="#">Link 2</a>
//                    <a href="#">Link 3</a>
//                </div>
//            </li>
//');

}

