<?php 
namespace App\Traits;

trait CustomTrait
{
    public function getSlug($title,$obj){
        $slug = \Str::slug($title);
        if($obj->where('slug',$slug)->count()){
            $slug .= rand(0,999);
        }
        return $slug;
    }
}