<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','parent_id','summary','image','status','added_by'];
    public  function getAllParentCats(){
        return $this->where('parent_id',NULL)->get();
    }
    public function categoryRules($act = 'add'){
        $rules =  ['title' => ['required','string','max:150','min:1'],
            'summary' => ['nullable','string'],
            'parent_id' => ['nullable','exists:categories,id'],
            'image' => ['required','image','max:5000'],
            'status' => ['required','in:active,inactive']
        ];
        if($act != 'add'){
            $rules['image'] = ['sometimes','image','max:5000'];
        }
        return $rules;
    }
    public function getSlug($title){
        $slug = \Str::slug($title);
        if($this->where('slug',$slug)->count()){
            $slug .= rand(0,99);
        }
        return $slug;
    }
    public function getsubCategories(){
        return $this->hasMany('\App\Models\Category','parent_id','id');
    }
/*    public function getChildCategories($cat_id){
        return $this->where('parent_id',$cat_id)->get();
    }*/
    public function getAllCategories(){
        return $this->where('parent_id',null)->where('status','active')->orderBy('title','ASC')->get();
    }
}
