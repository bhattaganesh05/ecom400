<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','summary', 'description', 'status', 'image', 'added_by'];
    public function getRules($act = 'add'){
        $rules =  [
            'title' => ["required",'string','max:100'],
            'summary' => ["required","string"],
            'description' => ["nullable","string"],
            'status' => ["required","in:active,inactive"],
            'image' => ["required","image","max:5000"]
        ];
        if($act != 'add'){
            $rules['image'] = ["sometimes","image","max:5000"];
        }
        return $rules;
    }
    public function getBlog($no_of_blog = null){
        if($no_of_blog != null){
            $blogs = $this->where('status','active')->orderBy('id','DESC')->limit(3)->get();
        }else{
            $blogs = $this->where('status','active')->orderBy('id','DESC')->get();
        }
        return $blogs;
    }
}
