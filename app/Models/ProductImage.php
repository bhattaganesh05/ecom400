<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    protected $fillable = ['product_id','image'];
    public function deleteImageByName($image_name){
        $image_data = $this->where('image',$image_name)->first();
        if($image_data){
            $status = $image_data->delete();
            if($status){
                deleteImage($image_name,'product');
            }
        }
    }
}
