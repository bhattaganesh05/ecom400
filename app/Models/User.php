<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function userInfo(){
        return $this->hasOne('\App\Models\UserInfo','user_id','id');
    }
    public  function getUpdateRule(){
        return [
            'name' => 'required|string|max:50',
            'address' => 'nullable|string|max:100',
            'phone' => 'nullable|string|max:20',
            'image' => 'sometimes|image|max:5000',
            'status' => 'sometimes|in:active,suspend'
        ];
    }
    public function getUpdatePassword(){
        return [
            'current_password' => ['required','string','min:8'],
            'password' => ['required','string','min:8','confirmed']
        ];
    }
    public  function getUserByRole($role){
        return $this->where('role',$role)->orderBy('name','ASC')->get();
    }
    public function getAllUsers(){
        return $this->where('id','!=',request()->user()->id)->orderBy('id','DESC')->get();
    }
    public function getRules(){
        return [
            'name' => 'required|string|max:50',
            'email' =>'required|email|unique:users,email',
            'password' => ['required','string','min:8','confirmed'],
//            'user_id'=>'required|exists:users,id',
            'phone' => 'required|string|max:20',
            'address' => 'required|string|max:100',
            'role' => 'required|in:customer,seller',
            'status' => 'required|in:active,suspend',
            'image' => 'required|image|max:5000'
        ];
    }
}
