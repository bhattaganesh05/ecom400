<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCollection extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','image','status','added_by'];
    public function getRules($act = 'add'){
        $rules =  [
            'title' => ['required','string','max:150','min:1'],
            'image' => ['required','image','max:5000'],
            'status' => ['required','in:active,inactive'],
            'product_id.*' => ['required','exists:products,id']
        ];
        if($act != 'add'){
            $rules['image'] = ['sometimes','image','max:5000'];
        }
        return $rules;
    }
    public function products(){
        return $this->hasMany('App\Models\CollectionProduct','collection_id','id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','added_by','id');
    }
    public function getLatestCollection(){
        return $this->where('status','active')->orderBy('id','DESC')->get();
    }
}
