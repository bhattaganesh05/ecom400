<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','image','status','added_by'];
    public function brandRules($act = 'add'){
        $rules =  ['title' => ['required','string','max:150','min:1'],
            'image' => ['sometimes','image','max:5000'],
            'status' => ['required','in:active,inactive']
        ];
        if($act != 'add'){
            $rules['image'] = ['sometimes','image','max:5000'];
        }
        return $rules;
    }
    public function getSlug($title){
        $slug = \Str::slug($title);
        if($this->where('slug',$slug)->count()){
            $slug .= rand(0,99);
        }
        return $slug;
    }
    public function get(){
        return $this->orderBy('created_at','desc')->orderBy('id','desc')->get();
    }
}
