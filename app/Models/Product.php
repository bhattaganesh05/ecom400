<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','category_id','sub_category_id','summary','description','price','discount','actual_cost','stock','seller_id','brand_id','is_featured','status','added_by'];
    public function getRules($act = 'add'){
        $rules =  [
            'title' => ["required",'string','max:100'],
            'category_id' => ["required","exists:categories,id"],
            'sub_category_id' => ["nullable","exists:categories,id"],
            'summary' => ["required","string"],
            'description' => ["nullable","string"],
            'price' => ["required","numeric","min:100"],
            'discount' => ["nullable","numeric","min:0","max:85"],
            'stock' => ["nullable","numeric","min:0"],
            'seller_id' => ["required","exists:users,id"],
            'brand_id' => ["nullable","exists:brands,id"],
            'is_featured' => ["sometimes","in:1"],
            'status' => ["required","in:active,inactive"],
            'image.*' => ["required","image","max:5000"]
        ];
        if($act != 'add'){
            $rules['image.*'] = ["sometimes","image","max:5000"];
        }
        return $rules;
    }
    public function images(){
        return $this->hasMany('App\Models\ProductImage','product_id','id');
    }
    public function category(){
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
    public function subCategory(){
        return $this->belongsTo('App\Models\Category','sub_category_id','id');
    }
    public function getProductBySeller($seller_id){
        return $this->where('seller_id',$seller_id)->get();
    }
    public function getFeaturedProducts(){
        return $this->where('is_featured',true)->where('status','active')->orderBy('id','DESC')->limit(16)->get();
    }
    public function getProducts($no_of_product = null ){
        if($no_of_product !=  null){
            $products = $this->where('status','active')->orderBy('id','DESC')->limit(6)->get();
        }else{
            $products = $this->where('status','active')->orderBy('id','DESC')->paginate(12);
        }
        return $products;
    }
    public function productReviews(){
        return $this->hasMany('App\Models\ProductReview','product_id','id');
    }
    public function getTopRatedProducts(){
        return $this->where('status','active')->orderBy('id','DESC')->limit(6)->get();
    }
    public function getLatestProductsByCategoryId($cat_id){
        $products = $this->where('status','active')->where('category_id',$cat_id)->orderBy('id','DESC')->limit(6)->get();
        return $products;
    }
    public function getLatestProductsBySubCategoryId($sub_cat_id){
        $products = $this->where('status','active')->where('sub_category_id',$sub_cat_id)->orderBy('id','DESC')->limit(6)->get();
        return $products;
    }
}
