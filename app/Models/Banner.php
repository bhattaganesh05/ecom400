<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $fillable = ['title','link','image','status','added_by'];
    public function bannerRules($act = 'add'){
        $rules =  ['title' => ['required','string','max:150','min:1'],
            'link' => ['nullable','url'],
            'image' => ['required','image','max:5000'],
            'status' => ['required','in:active,inactive']
        ];
        if($act != 'add'){
            $rules['image'] = ['sometimes','image','max:5000'];
        }
        return $rules;
    }
    public function get(){
        return $this->orderBy('created_at','desc')->orderBy('id','desc')->get();
    }

}
