<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferProductCollection extends Model
{
    use HasFactory;
    use HasFactory;
    protected $fillable = ['title','slug','discount','image','status','added_by'];
    public function getRules($act = 'add'){
        $rules =  [
            'title' => ['required','string','max:150','min:1'],
            'discount' => ['required','numeric','min:0','max:90'],
            'image' => ['required','image','max:5000'],
            'status' => ['required','in:active,inactive'],
            'product_id.*' => ['required','exists:products,id']
        ];
        if($act != 'add'){
            $rules['image'] = ['sometimes','image','max:5000'];
        }
        return $rules;
    }
    public function products(){
        return $this->hasMany('\App\Models\OfferCollectionProduct','collection_id','id');
    }
}
