@extends('layouts.admin')
@section('title','Blog Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Blog Form </div>
                    <a href="{{ route('blog.index') }}" title="List All Blog" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                    @if(isset($blog_detail))
                        {{ Form::open(['url'=>route('blog.update',$blog_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    @else
                        {{ Form::open(['url'=>route('blog.store') ,'class'=>'form','files'=>true]) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::text('title',@$blog_detail->title,['class'=>'form-control form-control-sm','id'=>'title','required'=>true,'placeholder'=>'Enter title for blog']) }}
                            @error('title')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('summary','Summary',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('summary',@$blog_detail->summary,['class'=>'form-control form-control-sm','id'=>'summary','required'=>true,'placeholder'=>'Enter summary for blog','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('description','Description',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('description',@$blog_detail->description,['class'=>'form-control form-control-sm','id'=>'description','required'=>false,'placeholder'=>'Enter description for blog','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$blog_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                            @error('status')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                        <div class="form-group row">
                            {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                            <div class="col-sm-12 col-md-6">
                                {{ Form::file('image',['id'=>'image_update','required'=> false,'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                                @error('image')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                @if(isset($blog_detail) && !empty($blog_detail->image))
                                    <img src="{{ asset("uploads/blog/".$blog_detail->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                                @else
                                    <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                                @endif
                            </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-9 offset-md-3">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#category_id').change(function (e) {
            var cat_id = $(this).val();
            var sub_cat_id = "{{ isset($blog_detail) ? $blog_detail->sub_category_id : null }}";
            if(cat_id != null){
                $.ajax({
                    url: "{{ route('get-sub-cats') }}",
                    type: "post",
                    data: {
                        _token: "{{csrf_token()}}",
                        cat_id: cat_id
                    },
                    success: function (response) {
                        if(typeof(response) != 'object'){
                            response = JSON.parse(response);
                        }
                        var options = "<option value='' selected>--Select Any One--</option>";
                        if(response.status){
                            console.log(response.data);
                            $.each(response.data,function (index,sub_cat) {
                                options += "<option value = '"+sub_cat.id+"'";
                                if(sub_cat_id != null && sub_cat_id == sub_cat.id){
                                    options += "selected";
                                }
                                options += ">"+sub_cat.title+"</options>";
                            });
                            $('#sub_cat_div').removeClass('d-none');
                        }else{
                            $('#sub_cat_div').addClass('d-none');
                        }
                        $('#sub_category_id').html(options);
                    }
                });
            }
        });
        $('#category_id').change();
    </script>
@endsection



