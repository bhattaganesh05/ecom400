@extends('layouts.admin')
@section('title','Blog List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Blog List</div>
                    <a href="{{ route('blog.create') }}" title="Add Blog" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table ">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($blog_list->count())
                            @foreach($blog_list as $blog)
                                <tr>
                                    <td>{{$blog->title}}</td>
                                    <td>{{ $blog->summary }}</td>
                                    <td>
                                        @if($blog->image != null)
                                        <a href="{{ asset('uploads/blog/'.$blog->image) }}" data-lightbox = "image-{{ $blog->id }}" data-title = {{ $blog->title }}>Preview</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('blog-change-status',[$blog->id,$blog->status]) }}" class="badge badge-{{ $blog->status == 'active' ? 'success' : 'danger' }}">{{ $blog->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('blog.edit',$blog->id) }}" title="Edit this blog" class="btn btn-primary btn-sm btn-circle" >
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this blog" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('blog.destroy',$blog->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection




