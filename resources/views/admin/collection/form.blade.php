@extends('layouts.admin')
@section('title','Collection Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Collection Form </div>
                    <a href="{{ route('collection.index') }}" title="List All Collection" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                    @if(isset($collection_detail))
                        {{ Form::open(['url'=>route('collection.update',$collection_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    @else
                        {{ Form::open(['url'=>route('collection.store') ,'class'=>'form','files'=>true]) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::text('title',@$collection_detail->title,['class'=>'form-control form-control-sm','id'=>'title','required'=>true,'placeholder'=>'Enter title for collection']) }}
                            @error('title')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$collection_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                            @error('status')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-4">
                            {{ Form::file('image',['id'=>'image_update','required'=>(isset($collection_detail) ? false : true),'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                            @error('image')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            @if(isset($collection_detail) && !empty($collection_detail->image))
                                <img src="{{ asset("uploads/collection/".$collection_detail->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @else
                                <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @endif
                        </div>
                    </div>
                        <hr>
                    <div class="form-group row">
                        <div class="col-12">
                            <h5 class="text-left">Select Products for this Collection</h5>
                            <hr>
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('product_id','Products',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::select('product_id[]',$product_list,'',['class'=>'form-control form-control-sm','multiple'=>true,'required'=>true,'id'=>'product_id']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-7 offset-md-5">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var s2 = $('#product_id').select2();
        var data = {{ isset($collection_detail) ? $collection_detail->products->pluck('product_id') : '' }}
        s2.val(data).trigger('change');
    </script>
@endsection

