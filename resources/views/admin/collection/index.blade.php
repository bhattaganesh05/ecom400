@extends('layouts.admin')
@section('title','Collection List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Collection List</div>
                    <a href="{{ route('collection.create') }}" title="Add Collection" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Added_By</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($collection_list->count())
                            @foreach($collection_list as $collection_list)
                                <tr>
                                    <td>{{$collection_list->title}}</td>
                                    <td>{{ $collection_list->user->name }}</td>
                                    <td>
                                        <a href="{{ asset('uploads/collection/'.$collection_list->image) }}" data-lightbox = "image-{{ $collection_list->id }}" data-title = {{ $collection_list->title }}>Preview</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('collection-change-status',[$collection_list->id,$collection_list->status]) }}" class="badge badge-{{ $collection_list->status == 'active' ? 'success' : 'danger' }}">{{ $collection_list->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('collection.edit',$collection_list->id) }}" title="Edit this collection" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this collection" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('collection.destroy',$collection_list->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
