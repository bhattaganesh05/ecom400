@extends('layouts.admin')
@section('title','Offer offer_collection List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Offer List</div>
                    <a href="{{ route('offer.create') }}" title="Add Offer offer_collection" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Discount</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>

                        @if($offer_collection_list->count())
                            @foreach($offer_collection_list as $offer_collection_list)
                                <tr>
                                    <td>{{$offer_collection_list->title}}</td>
                                    <td>{{ $offer_collection_list->discount."%" }}</td>
                                    <td>
                                        <a href="{{ asset('uploads/offer/'.$offer_collection_list->image) }}" data-lightbox = "image-{{ $offer_collection_list->id }}" data-title = {{ $offer_collection_list->title }}>Preview</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('offer-change-status',[$offer_collection_list->id,$offer_collection_list->status]) }}" class="badge badge-{{ $offer_collection_list->status == 'active' ? 'success' : 'danger' }}">{{ $offer_collection_list->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('offer.edit',$offer_collection_list->id) }}" title="Edit this offer_collection" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>

                                        <a href="javascript:;" title="Delete this offer_collection" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('offer.destroy',$offer_collection_list->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
