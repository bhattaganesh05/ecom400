@extends('layouts.admin')
@section('title','Page Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Page Form </div>
                    <a href="{{ route('page.index') }}" title="List All Page" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                        {{ Form::open(['url'=>route('page.update',$page_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    <div class="form-group row">
                        {{ Form::label('summary','Summary',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('summary',@$page_detail->summary,['class'=>'form-control form-control-sm','id'=>'summary','required'=>true,'placeholder'=>'Enter summary for page','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('description','Description',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('description',@$page_detail->description,['class'=>'form-control form-control-sm','id'=>'description','required'=>false,'placeholder'=>'Enter description for page','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$page_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                            @error('status')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-5">
                            {{ Form::file('image',['id'=>'image_update','required'=>(isset($page_detail) ? false : true),'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                            @error('image')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            @if(isset($page_detail) && !empty($page_detail->image))
                                <img src="{{ asset("uploads/page/".$page_detail->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @else
                                <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-9 offset-md-3">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

