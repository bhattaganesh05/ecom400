@extends('layouts.admin')
@section('title','Page List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Page List</div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($page_list->count())
                            @foreach($page_list as $page)
                                <tr>
                                    <td>{{$page->title}}</td>
                                    <td>{{$page->summary}}</td>
                                    <td>
                                        @if($page->image != null)
                                        <a href="{{ asset('uploads/page/'.$page->image) }}" data-lightbox = "image-{{ $page->id }}" data-title = {{ $page->title }}>Preview</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('page-change-status',[$page->id,$page->status]) }}" class="badge badge-{{ $page->status == 'active' ? 'success' : 'danger' }}">{{ $page->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('page.edit',$page->id) }}" title="Edit this page" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
