@extends('layouts.admin')
@section('title','Category List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Category List</div>
                    <a href="{{ route('category.create') }}" title="Add Category" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table ">
                        <thead class="thead-dark">
                        {{--                            <th>S.N.</th>--}}
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($parent_cat_list->count())
                            @foreach($parent_cat_list as $category_list)
                                <tr>
                                    {{--                                    <td>{{$category_list->id}}</td>--}}
                                    <td>{{$category_list->title}}</td>
                                    <td><span>{{$category_list->summary}}</span></td>
                                    <td>
                                        <a href="{{ asset('uploads/category/'.$category_list->image) }}" data-lightbox = "image-{{ $category_list->id }}" data-title = {{ $category_list->title }}>Preview</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('category-change-status',[$category_list->id,$category_list->status]) }}" class="badge badge-{{ $category_list->status == 'active' ? 'success' : 'danger' }}">{{ $category_list->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" data-cat_id = "{{ $category_list->id }}" title="view  child categories " class="btn btn-secondary btn-sm btn-circle view-sub-cat">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('category.edit',$category_list->id) }}" title="Edit this category" class="btn btn-primary btn-sm btn-circle" >
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this category" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('category.destroy',$category_list->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- Sub-category view   Modal -->
<div class="modal fade" id="subCategoryList" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sub-category list</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <table class="table table-striped table-hover data-table ">
                    <thead class="thead-dark">
                    <th>Title</th>
                    <th>Summary</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody id="sub_cat_body">
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $('.view-sub-cat').click(function (e) {
            e.preventDefault();
            let cat_id = $(this).data('cat_id');
            $.ajax({
                'url': "{{ route('get-sub-cats') }}",
                'type': "post",
                'data': {
                    _token: "{{ csrf_token() }}",
                    cat_id: cat_id
                },
                success: function (response) {
                     console.log(response);
                     if(typeof(response) != "object"){
                         response = JSON.parse(response);
                     }
                     if(response.status){
                         var tbody_html = "";
                         $.each(response.data,function (key,sub_cat_detail){
                             tbody_html += "<tr>";
                             tbody_html += "<td>"+sub_cat_detail.title+"</td>";
                             tbody_html += "<td>"+sub_cat_detail.summary+"</td>";
                             tbody_html += "<td><a href='{{ asset('uploads/category') }}/"+sub_cat_detail.image+"' data-lightbox = 'image-'"+sub_cat_detail.id+"' data-title = '"+sub_cat_detail.title+"'>Preview</a></td>";
                             tbody_html += "<td>";
                             tbody_html += "<a href='/admin/category/status-change/"+sub_cat_detail.id+"/"+sub_cat_detail.status+"' class='badge badge-"+(sub_cat_detail.status == 'active' ? 'success' : 'danger')+"'>"+(sub_cat_detail.status == 'active' ? 'Published' : 'Un-published')+"</a></td>";
                             tbody_html += "<td><a href='/admin/category/"+sub_cat_detail.id+"/edit'  title='Edit this category' class='btn btn-primary btn-sm btn-circle'><i class='fa fa-pen'></i></a>";
                             tbody_html += " <a href='javascript:;' title='Delete this category'  onclick='deleteThisCategory(this)' class='btn btn-warning btn-sm btn-circle btn-delete'><i class='fa fa-trash'></i></a>";
                             tbody_html += "<form method='post' action = '/admin/category/"+sub_cat_detail.id+"' class = 'delete-form'>";
                             tbody_html += '@csrf';
                             tbody_html += '@method('delete')';
                             tbody_html += "</form>";
                             tbody_html += "</td>";
                             tbody_html += "/<tr>";
                         });
                         $('#sub_cat_body').html(tbody_html);
                         $('#subCategoryList').modal('show');
                     }else{
                         alert('No sub-categories found in this category.');
                     }
                }
            });
        });
    </script>
@endsection

