@extends('layouts.admin')
@section('title','User List || Ecommerce Website')
@section('main_content')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">All {{ isset($is_all) ? '' : ucfirst($role) }} User List</div>
                        <a href="{{ route('user.create') }}" title="Add User" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover data-table">
                            <thead class="thead-dark">
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            @if(isset($is_all))
                            <th>Role</th>
                            @endif
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            @if($all_users->count())
                                @foreach($all_users as $data)
                                <tr>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{ @$data->userInfo->phone }}</td>
                            @if(isset($is_all))
                                    <td>
                                        {{ $data->role }}
                                    </td>
                                @endif
                                    <td>
                                    @if($data->userInfo)
                                        <a href="{{ asset('uploads/user/'.$data->userInfo->image) }}" data-lightbox = "image-{{ $data->id }}" data-title = {{ $data->title }}>Preview</a>
                                    @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('user-change-status',[$data->id,$data->status]) }}" class="badge badge-{{ $data->status == 'active' ? 'success' : 'danger' }}">{{ $data->status == 'active' ? 'Active' : 'Inactive' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('change-pwd',$data->id) }}" title="Update Password" class="btn btn-secondary btn-sm btn-circle">
                                            <i class="fa fa-key"></i>
                                        </a>
                                        <a href="{{ route('user.edit',$data->id) }}" title="Edit this user" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this user" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('user.destroy',$data->id) , 'class' => 'delete-form' ]) }}
                                            @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
