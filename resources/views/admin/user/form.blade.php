@extends('layouts.admin')
@section('title','User Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">User Form </div>
                    <a href="{{ route('user.index') }}" title="List All User" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                    @if(isset($user_detail))
                        {{ Form::open(['url'=>route('user.update',$user_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    @else
                        {{ Form::open(['url'=>route('user.store') ,'class'=>'form','files'=>true]) }}
                    @endif
                        <div class="form-group row">
                            {{ Form::label('name','Name',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::text('name',@$user_detail->name,['class'=>'form-control form-control-sm','id'=>'name','required'=>true,'placeholder'=>'Enter your name']) }}
                                @error('name')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        @if(!isset($user_detail))
                        <div class="form-group row">
                            {{ Form::label('email','Email Address',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::text('email',@$user_detail->email,['class'=>'form-control form-control-sm','id'=>'email','required'=>true,'placeholder'=>'Enter user email address']) }}
                                @error('email')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('password','Password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::password('password',['class'=>'form-control form-control-sm','id'=>'password','required'=>true,'placeholder'=>'Enter  password']) }}
                                @error('password')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('confirmation_password','Re-password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::password('password_confirmation',['class'=>'form-control form-control-sm','id'=>'confirmation_password','required'=>true,'placeholder'=>'Re-enter  password ']) }}
                                @error('confirmation_password')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            {{ Form::label('phone','Phone',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::tel('phone',@$user_detail->userInfo->phone,['class'=>'form-control form-control-sm','id'=>'phone','required'=>false,'placeholder'=>'Enter your phone number']) }}
                                @error('phone')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    <div class="form-group row">
                        {{ Form::label('address','Address',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::text('address',@$user_detail->userInfo->address,['class'=>'form-control form-control-sm','id'=>'address','required'=>false,'placeholder'=>'Enter your address']) }}
                            @error('address')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                        <div class="form-group row">
                            {{ Form::label('role','Role',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::select('role',['customer'=>"Customer",'seller'=>"Seller"],@$user_detail->role,['class'=>'form-control form-control-sm','id'=>'role','required'=>true]) }}
                                @error('role')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::select('status',['active'=>"Active",'suspend'=>"Inactive"],@$user_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                                @error('status')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    <div class="form-group row">
                        {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-4">
                            {{ Form::file('image',['id'=>'image_update','required'=>(isset($user_detail) ? false : true),'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                            @error('image')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            @if(isset($user_detail) && !empty($user_detail->userInfo->image))
                                <img src="{{ asset("uploads/user/".$user_detail->userInfo->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @else
                                <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-7 offset-md-5">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

