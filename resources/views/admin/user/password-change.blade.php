@extends('layouts.admin')
@section('title','User Password Change Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">User Password Change Form </div>
                </div>
                <div class="ibox-body">
                    {{ Form::open(['url'=>route('user-update-password',$user_data->id) ,'class'=>'form']) }}
                        @method('patch')
                        <div class="form-group row">
                            {{ Form::label('password','New Password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::password('password',['class'=>'form-control form-control-sm','id'=>'password','required'=>true,'placeholder'=>'Enter  password']) }}
                                @error('password')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('confirmation_password','Re-type New password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::password('password_confirmation',['class'=>'form-control form-control-sm','id'=>'confirmation_password','required'=>true,'placeholder'=>'Re-enter  password ']) }}
                                @error('confirmation_password')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-7 offset-md-5">
                                <button type="submit" class="btn btn-success btn-block btn-sm" >Update Password</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

