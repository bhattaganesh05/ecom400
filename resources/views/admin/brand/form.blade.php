@extends('layouts.admin')
@section('title','Brand Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Brand Form </div>
                    <a href="{{ route('brand.index') }}" title="List All Brand" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                    @if(isset($brand_detail))
                        {{ Form::open(['url'=>route('brand.update',$brand_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    @else
                        {{ Form::open(['url'=>route('brand.store') ,'class'=>'form','files'=>true]) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::text('title',@$brand_detail->title,['class'=>'form-control form-control-sm','id'=>'title','required'=>true,'placeholder'=>'Enter title for brand']) }}
                            @error('title')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-7">
                            {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$brand_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                            @error('status')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                        <div class="col-sm-12 col-md-4">
                            {{ Form::file('image',['id'=>'image_update','required'=> false,'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                            @error('image')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            @if(isset($brand_detail) && !empty($brand_detail->image))
                                <img src="{{ asset("uploads/brand/".$brand_detail->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @else
                                <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-7 offset-md-5">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection



