@extends('layouts.admin')
@section('title','Brand List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Brand List</div>
                    <a href="{{ route('brand.create') }}" title="Add Brand" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table ">
                        <thead class="thead-dark">
                        {{--                            <th>S.N.</th>--}}
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($brand_list->count())
                            @foreach($brand_list as $brand_list)
                                <tr>
                                    {{--                                    <td>{{$brand_list->id}}</td>--}}
                                    <td>{{$brand_list->title}}</td>
                                    <td><span>{{$brand_list->summary}}</span></td>
                                    <td>
                                        <a href="{{ asset('uploads/brand/'.$brand_list->image) }}" data-lightbox = "image-{{ $brand_list->id }}" data-title = {{ $brand_list->title }}>Preview</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('brand-change-status',[$brand_list->id,$brand_list->status]) }}" class="badge badge-{{ $brand_list->status == 'active' ? 'success' : 'danger' }}">{{ $brand_list->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('brand.edit',$brand_list->id) }}" title="Edit this brand" class="btn btn-primary btn-sm btn-circle" >
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this brand" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('brand.destroy',$brand_list->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- Sub-brand view   Modal -->
<div class="modal fade" id="subBrandList" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sub-brand list</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <table class="table table-striped table-hover data-table ">
                    <thead class="thead-dark">
                    <th>Title</th>
                    <th>Summary</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody id="sub_cat_body">
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>



