<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                @if(isset(request()->user()->userInfo['image']) && !empty(request()->user()->userInfo['image']))
                    <img src="{{asset('uploads/user/'.request()->user()->userInfo['image'])}}" width="45px" class="img-circle"/>
                @else
                    <img src="{{asset('img/default_user.jpg')}}"  width="45px" />
                @endif
            </div>
            <div class="admin-info">
                <div class="font-strong">{{auth()->user()->name}}</div><small>{{ucfirst(auth()->user()->role)}}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{route(request()->user()->role)}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-images"></i>
                    <span class="nav-label">Banner Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('banner.create') }}">Add Banner</a>
                    </li>
                    <li>
                        <a href="{{ route('banner.index') }}">List Banner</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bold"></i>
                    <span class="nav-label">Brand Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('brand.create') }}">Add Brand</a>
                    </li>
                    <li>
                        <a href="{{ route('brand.index') }}">List Brand</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Category Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('category.create') }}">Add Category</a>
                    </li>
                    <li>
                        <a href="{{ route('category.index') }}">List Category</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-bag"></i>
                    <span class="nav-label">Product Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('product.create') }}">Add Product</a>
                    </li>
                    <li>
                        <a href="{{ route('product.index') }}">List Product</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-basket"></i>
                    <span class="nav-label">Collection Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('collection.create') }}">Add Collection</a>
                    </li>
                    <li>
                        <a href="{{ route('collection.index') }}">List Collection</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-box-open"></i>
                    <span class="nav-label">Offers Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('offer.create') }}">Add Offers</a>
                    </li>
                    <li>
                        <a href="{{ route('offer.index') }}">List Offers</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">User Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('user.create') }}">Add User</a>
                    </li>
                    <li>
                        <a href="{{ route('user.index') }}">List All User</a>
                    </li>
                    <li>
                        <a href="{{ route('user.show','customer') }}">List Customer</a>
                    </li>
                    <li>
                        <a href="{{ route('user.show','seller') }}">List Seller</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
                    <span class="nav-label">Order Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Order</a>
                    </li>
                    <li>
                        <a href="cards.html">List Order</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-comments"></i>
                    <span class="nav-label">Review Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Review</a>
                    </li>
                    <li>
                        <a href="cards.html">List Review</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-file"></i>
                    <span class="nav-label">Pages Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('page.create') }}">Add Pages</a>
                    </li>
                    <li>
                        <a href="{{ route('page.index') }}">List Pages</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-file"></i>
                    <span class="nav-label">Blogs Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('blog.create') }}">Add blog</a>
                    </li>
                    <li>
                        <a href="{{ route('blog.index') }}">List blog</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-cogs"></i>
                    <span class="nav-label">Miscellaneous Setting</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Review</a>
                    </li>
                    <li>
                        <a href="cards.html">List Review</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-motorcycle"></i>
                    <span class="nav-label">Delivery Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Delivery Options</a>
                    </li>
                    <li>
                        <a href="cards.html">List Delivery Options</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
