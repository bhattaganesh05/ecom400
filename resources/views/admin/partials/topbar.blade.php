<header class="header">
    <div class="page-brand">
        <a class="link" href="{{route(request()->user()->role)}}">
            <span class="brand">Ecommerce
                <span class="brand-tip">CMS</span>
            </span>
            <span class="brand-mini">AC</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
            </li>
        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li class="dropdown dropdown-inbox">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i>
                    <span class="badge badge-primary envelope-badge">0</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                    <li class="dropdown-menu-header">
                        <div>
                            <span><strong>9 New</strong> Orders</span>
                            <a class="pull-right" href="mailbox.html">view all</a>
                        </div>
                    </li>
                    <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                        <div>
                            <a class="list-group-item">
                                <div class="media">
                                    <div class="media-img">
                                        <!-- <img src="./assets/img/users/u1.jpg" /> -->
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong"> </div>Jeanne Gonzalez<small class="text-muted float-right">Just now</small>
                                        <div class="font-13">Your proposal interested me.</div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    @if(isset(request()->user()->userInfo['image']) && !empty(request()->user()->userInfo['image']))
                        <img src="{{asset('uploads/user/'.request()->user()->userInfo['image'])}}"  class="img-circle"/>
                    @else
                        <img src="{{asset('img/default_user.jpg')}}" class="img-circle"/>
                    @endif
                    <span>{{auth()->user()->name}}</span><i class="fa fa-angle-down m-l-5"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item edit-profile" href="javascript:;"><i class="fa fa-user"></i>Profile</a>
                    <a class="dropdown-item update-password" href="javascript:;"><i class="fa fa-key"></i>Change Password</a>
                    <li class="dropdown-divider"></li>
                    <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logoutForm').submit()">
                        {{ Form::open( ['url'=> route('logout'),'id'=>'logoutForm']) }}
                        {{ Form::close() }}
                    <i class="fa fa-power-off"></i>Logout</a>
                </ul>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>
