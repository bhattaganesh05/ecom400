@extends('layouts.admin')
@section('title','Banner Form || Ecommerce Website')
@section('main_content')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Banner Form </div>
                        <a href="{{ route('banner.index') }}" title="List All Banner" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                    </div>
                    <div class="ibox-body">
                        @if(isset($banner_detail))
                            {{ Form::open(['url'=>route('banner.update',$banner_detail->id) ,'class'=>'form','files'=>true]) }}
                            @method('patch')
                        @else
                            {{ Form::open(['url'=>route('banner.store') ,'class'=>'form','files'=>true]) }}
                        @endif
                            <div class="form-group row">
                            {{ Form::label('title','Title',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::text('title',@$banner_detail->title,['class'=>'form-control form-control-sm','id'=>'title','required'=>true,'placeholder'=>'Enter title for banner']) }}
                            @error('title')
                                <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('link','Link',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::url('link',@$banner_detail->link,['class'=>'form-control form-control-sm','id'=>'link','required'=>false,'placeholder'=>'Enter link for banner']) }}
                                @error('link')
                                    <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row">
                            {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-7">
                                {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$banner_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                                @error('status')
                                    <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                            <div class="col-sm-12 col-md-4">
{{--                                <input type="file"  onchange="readURL(this,'thumb')" name="image" id="image_update" accept="image/*">--}}
                                {{ Form::file('image',['id'=>'image_update','required'=>(isset($banner_detail) ? false : true),'accept'=>"image/*",'onchange'=>"readURL(this,'thumb')"]) }}
                                @error('image')
                                    <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                @if(isset($banner_detail) && !empty($banner_detail->image))
                                    <img src="{{ asset("uploads/banner/".$banner_detail->image) }}" alt="" id="thumb" class="img-fluid img-thumbnail">
                                @else
                                    <img src="" alt="" id="thumb" class="img-fluid img-thumbnail">
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-7 offset-md-5">
                                <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                            </div>
                        </div>
                 {{ Form::close() }}
                    </div>
                    </div>
                </div>
            </div>
@endsection

