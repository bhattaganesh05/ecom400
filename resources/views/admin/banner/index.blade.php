@extends('layouts.admin')
@section('title','Banner List || Ecommerce Website')
@section('main_content')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">All Banner List</div>
                        <a href="{{ route('banner.create') }}" title="Add Banner" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped table-hover data-table">
                            <thead class="thead-dark">
{{--                            <th>S.N.</th>--}}
                            <th>Title</th>
                                <th>Link</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            @if($banner_list->count())
                                @foreach($banner_list as $banner_list)
                                <tr>
{{--                                    <td>{{$banner_list->id}}</td>--}}
                                    <td>{{$banner_list->title}}</td>
                                    <td><a href="{{$banner_list->link}}" class="btn btn-link" target="banner">{{$banner_list->link}}</a></td>
                                    <td>
                                        <a href="{{ asset('uploads/banner/'.$banner_list->image) }}" data-lightbox = "image-{{ $banner_list->id }}" data-title = {{ $banner_list->title }}>Preview</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('banner-change-status',[$banner_list->id,$banner_list->status]) }}" class="badge badge-{{ $banner_list->status == 'active' ? 'success' : 'danger' }}">{{ $banner_list->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('banner.edit',$banner_list->id) }}" title="Edit this banner" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this banner" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('banner.destroy',$banner_list->id) , 'class' => 'delete-form' ]) }}
                                            @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
