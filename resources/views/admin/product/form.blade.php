@extends('layouts.admin')
@section('title','Product Form || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Product Form </div>
                    <a href="{{ route('product.index') }}" title="List All Product" class="btn btn-info btn-sm btn-circle"><i class="fa fa-eye"></i></a>
                </div>
                <div class="ibox-body">
                    @if(isset($product_detail))
                        {{ Form::open(['url'=>route('product.update',$product_detail->id) ,'class'=>'form','files'=>true]) }}
                        @method('patch')
                    @else
                        {{ Form::open(['url'=>route('product.store') ,'class'=>'form','files'=>true]) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::text('title',@$product_detail->title,['class'=>'form-control form-control-sm','id'=>'title','required'=>true,'placeholder'=>'Enter title for product']) }}
                            @error('title')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('summary','Summary',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('summary',@$product_detail->summary,['class'=>'form-control form-control-sm','id'=>'summary','required'=>true,'placeholder'=>'Enter summary for product','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('description','Description',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::textarea('description',@$product_detail->description,['class'=>'form-control form-control-sm','id'=>'description','required'=>false,'placeholder'=>'Enter description for product','rows'=>3,'style'=> 'resize:none;']) }}
                            @error('summary')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('category_id','Category',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('category_id',$category_list,@$product_detail->category_id,['class'=>'form-control form-control-sm','id'=>'category_id','required'=>false,'placeholder'=>'--Choose any choose if needed--']) }}
                            @error('category_id')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row d-none" id="sub_cat_div">
                        {{ Form::label('sub_category_id','Sub-Category',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('sub_category_id',[],'',['class'=>'form-control form-control-sm','id'=>'sub_category_id','required'=>false]) }}
                            @error('sub_category_id')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('price','Price (NPR.)',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::number('price',@$product_detail->price,['class'=>'form-control form-control-sm','id'=>'price','required'=>true,'placeholder'=>'Enter price for product','min'=>100]) }}
                            @error('price')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('discount','Discount (%)',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::number('discount',@$product_detail->discount,['class'=>'form-control form-control-sm','id'=>'discount','required'=>false,'placeholder'=>'Enter discount for product','min'=>0,'max'=>85]) }}
                            @error('discount')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('stock','Stock (in no.)',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::number('stock',@$product_detail->stock,['class'=>'form-control form-control-sm','id'=>'stock','required'=>false,'placeholder'=>'Enter stock for product','min'=>0]) }}
                            @error('stock')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('brand_id','Brand',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('brand_id',$brand_list,@$product_detail->brand_id,['class'=>'form-control form-control-sm','id'=>'brand_id','required'=>false,'placeholder'=>'--Choose any product if needed--']) }}
                            @error('brand_id')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                        <div class="form-group row">
                            {{ Form::label('seller_id','Seller',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                            <div class="col-sm-12 col-md-9">
                                {{ Form::select('seller_id',$seller_list,@$product_detail->seller_id,['class'=>'form-control form-control-sm','id'=>'seller_id','required'=>false,'placeholder'=>'--Choose seller if needed--']) }}
                                @error('seller_id')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('status',['active'=>"Published",'inactive'=>"Un-published"],@$product_detail->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true]) }}
                            @error('status')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                        <div class="form-group row">
                            {{ Form::label('is_featured','Featured',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                            <div class="col-sm-12 col-md-9">
                                {{ Form::checkbox('is_featured',1,@$product_detail->is_featured,['id'=>'is_featured','required'=>false,]) }} Yes
                                @error('is_featured')
                                <span class="alert alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    <div class="form-group row">
                        {{ Form::label('image_update','Image',['class'=>'col-sm-12 col-md-3 text-md-right']) }}
                        <div class="col-sm-12 col-md-4">
                            {{ Form::file('image[]',['id'=>'gallery-photo-add','multiple'=>true,'required'=>(isset($product_detail) ? false : true),'accept'=>"image/*",'onchange'=>"imagesPreview(this,'div.gallery')"]) }}
                            @error('image')
                            <span class="alert alert-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @if(isset($product_detail) && !empty($product_detail->images))
                    <div class="form-group row">
                            @foreach($product_detail->images as $image)
                                    <div class="col-md-2 mt-1">
                                        <a href="{{ asset("uploads/product/".$image->image) }} " data-lightbox = "image-1" data-title = {{ $product_detail->title }}>
                                        <img src="{{ asset("uploads/product/".$image->image) }}" alt="" class="img-fluid img-thumbnail">
                                        </a>
                                        {{ Form::checkbox('del_image[]',$image->image,false,) }} <span class="text-danger">Delete</span>
                                    </div>
                                @endforeach
                            </div>
                        @else
                                <div class="form-group row gallery"></div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-9 offset-md-3">
                            <button type="submit" class="btn btn-success btn-block btn-sm" >Submit</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#category_id').change(function (e) {
            var cat_id = $(this).val();
            var sub_cat_id = "{{ isset($product_detail) ? $product_detail->sub_category_id : null }}";
            if(cat_id != null){
                $.ajax({
                    url: "{{ route('get-sub-cats') }}",
                    type: "post",
                    data: {
                        _token: "{{csrf_token()}}",
                        cat_id: cat_id
                    },
                    success: function (response) {
                        if(typeof(response) != 'object'){
                            response = JSON.parse(response);
                        }
                        var options = "<option value='' selected>--Select Any One--</option>";
                        if(response.status){
                            console.log(response.data);
                            $.each(response.data,function (index,sub_cat) {
                                options += "<option value = '"+sub_cat.id+"'";
                                if(sub_cat_id != null && sub_cat_id == sub_cat.id){
                                    options += "selected";
                                }
                                options += ">"+sub_cat.title+"</options>";
                            });
                            $('#sub_cat_div').removeClass('d-none');
                        }else{
                            $('#sub_cat_div').addClass('d-none');
                        }
                        $('#sub_category_id').html(options);
                    }
                });
            }
        });
        $('#category_id').change();
    </script>
@endsection



