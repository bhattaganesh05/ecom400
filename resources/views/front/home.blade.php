@extends('layouts.app')
@section('title','Home Page | Ecommerce Website')
@section('banner')
<div class="col-lg-9">
    <div class="hero__search">
        <div class="hero__search__form">
            <form action="{{ route('search') }}" >
                <input type="text" name="keyword" placeholder="What do yo u need?">
                <button type="submit" class="site-btn">SEARCH</button>
            </form>
        </div>
        <div class="hero__search__phone">
            <div class="hero__search__phone__icon">
                <i class="fa fa-phone"></i>
            </div>
            <div class="hero__search__phone__text">
                <h5>+977 9848979669</h5>
                <span>support 24/7 time</span>
            </div>
        </div>
    </div>
    @if($banner_data)
        <a href="{{ $banner_data->link }}" target="_blank">
    <div class="hero__item set-bg" data-setbg="{{ asset('uploads/banner/'.$banner_data->image) }}" style="background-position: top cover;">
{{--        <div class="hero__text">--}}
{{--            <span>FRUIT FRESH</span>--}}
{{--            <h2>Vegetable <br />100% Organic</h2>--}}
{{--            <p>Free Pickup and Delivery Available</p>--}}
{{--            <a href="#" class="primary-btn">SHOP NOW</a>--}}
{{--        </div>--}}
    </div>
        </a>
    @endif
</div>
@endsection
@section('content')
<!-- Categories Section Begin -->
<section class="categories">
    <div class="container">
        <div class="row">
            <div class="categories__slider owl-carousel">
                @if(isset($parent_cats) && $parent_cats->count())
                    @foreach($parent_cats as $category)
                        <div class="col-lg-3">
                            <div class="categories__item set-bg" data-setbg="{{asset('uploads/category/' .$category->image)}}">
                                <h5><a href="{{ route('cat-products',$category->slug) }}">{{$category->title}}</a></h5>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!-- Categories Section End -->
<!-- Featured Section Begin -->
<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Featured Product</h2>
                </div>
                <div class="featured__controls">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        @if($parent_cats->count())
                            @foreach($parent_cats as $cat)
                        <li data-filter="{{ ".".$cat->slug }}">{{ $cat->title }}</li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="row featured__filter">
            @if($featured_products->count())
                @foreach($featured_products as $product)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix {{ $product->category->slug }}">
                        <div class="featured__item">
                            <div class="featured__item__pic set-bg" data-setbg="{{ asset('uploads/product/'.$product->images[0]->image) }}">
                                <ul class="featured__item__pic__hover">
                                    <li><a href="{{ route('product-detail',$product->slug) }}" title = "View Detail"><i class="fa fa-eye"></i></a></li>
                                    <li><a href="#" title=" Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul>
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="#"> {{ $product->title }}</a></h6>
                                <h5>{{"NPR.".number_format($product->actual_cost) }}
                                @if($product->discount)
                                    (<del class="text-danger">{{ $product->price }}</del>)
                                    @endif
                                </h5>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
    </div>
    </div>
</section>
<!-- Featured Section End -->
<!-- Banner Begin -->
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="banner__pic">
                    <img src="img/banner/banner-1.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="banner__pic">
                    <img src="img/banner/banner-2.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner End -->
<!-- Latest Product Section Begin -->
<section class="latest-product spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Latest Products</h4>
                    <div class="latest-product__slider owl-carousel">
                        @if($latest_products)
                            @foreach($latest_products as $key => $product)
                                @if( $key == 0 || $key == 3)
                                    <div class="latest-prdouct__slider__item">
                                        @endif
                                        <a href="{{ route('product-detail',$product->slug) }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('uploads/product/'.$product->images[0]->image) }}" alt="" style="width: auto;">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>{{ $product->title }}</h6>
                                                <span>{{"NPR.".number_format($product->actual_cost) }}
                                                    @if($product->discount)
                                                        <del class="text-danger">{{ "( ".$product->price." )" }}</del>
                                                    @endif
                                                </span>
                                            </div>
                                        </a>
                                        @if( $key == 2 || $loop->last)
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Product Collections</h4>
                    <div class="latest-product__slider owl-carousel">
                        @if($latest_collections)
                            @foreach($latest_collections as $key => $collection)
                                @if( $key == 0 || $key == 3)
                        <div class="latest-prdouct__slider__item">
                            @endif
                            <a href="{{ route('collection-detail',$collection->slug) }}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{ asset('uploads/collection/'.$collection->image) }}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{ $collection->title }}</h6>
                                </div>
                            </a>
                            @if( $key == 2 || $loop->last)
                        </div>
                            @endif
                            @endforeach
                            @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Top Rated Products</h4>
                    <div class="latest-product__slider owl-carousel">
                        @if($top_rated_products)
                            @foreach($top_rated_products as $key => $product)
                                @if( $key == 0 || $key == 3)
                                    <div class="latest-prdouct__slider__item">
                                        @endif
                                        <a href="{{ route('product-detail',$product->slug) }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('uploads/product/'.$product->images[0]->image) }}" alt="" style="width: auto;">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>{{ $product->title }}</h6>
                                                <span>{{"NPR.".number_format($product->actual_cost) }}
                                                    @if($product->discount)
                                                        <del class="text-danger">{{ "( ".$product->price." )" }}</del>
                                                    @endif
                                                </span>
                                            </div>
                                        </a>
                                        @if( $key == 2 || $loop->last)
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Latest Product Section End -->
<!-- Blog Section Begin -->
<section class="from-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title from-blog__title">
                    <h2>From The Blog</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @if($top_blogs)
                @foreach($top_blogs as $key => $blog)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="{{ asset('uploads/blog/'.$blog->image) }}" alt="" height="200" style="width:auto; ">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="far fa-calendar"></i> {{ \Carbon\Carbon::parse($blog->created_at)->format('M   d, Y') }}</li>
                                    <li><i class="far fa-comment"></i> 5</li>
                                </ul>
                                <h5><a href="{{ route('blog-detail',$blog->slug) }}">{{ $blog->title }}</a></h5>
                                <p>{{ $blog->summary }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
<!-- Blog Section End -->
@endsection
