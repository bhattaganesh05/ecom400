<div class="col-lg-3">
    <div class="hero__categories">
        <div class="hero__categories__all">
            <i class="fa fa-bars"></i>
            <span>All Categories</span>
        </div>
        <ul>
            {!! getCategoryMenu() !!}
        </ul>
    </div>
</div>
