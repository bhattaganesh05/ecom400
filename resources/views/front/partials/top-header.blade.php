<!-- Humberger Begin -->
<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
        <a href="#"><img src="{{ asset('img/logo.png') }}" alt=""></a>
    </div>
    <div class="humberger__menu__cart">
        <ul>
            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>0</span></a></li>
        </ul>
        <div class="header__cart__price">item: <span>NPR. {{ number_format(0) }}</span></div>
    </div>
    <div class="humberger__menu__widget">
        <div class="header__top__right__auth">
            @guest
                @if (Route::has('register'))
                    <a href="{{ route('login') }}" title="SignUp"><i class="fa fa-user-plus"></i></a>
                @endif
                @if (Route::has('login'))
                    <a href="{{ route('login') }}" title="SignIn"><i class="fa fa-sign-in-alt"></i></a>
                @endif
            @else
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                        document.getElementById('logout-form1').submit();">
                                {{ __('Logout') }} <i class="fa fa-power-off"></i>
                            </a>
                            <form id="logout-form1" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            @endguest
        </div>
    </div>
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li class=" @if(request()->getRequestUri() == '/')active @endif "><a href="{{ route('landing') }}">Home</a></li>
            <li class = " @if(request()->getRequestUri() == '/products')active @endif "><a href="{{ route('all-products') }}">Shop</a></li>
            <li class = " @if(request()->getRequestUri() == '/pages/about-us')active @endif "><a href="{{ route('page-detail','about-us') }}">Pages</a>
                <ul class="header__menu__dropdown ">
                    <li class = " @if(request()->getRequestUri() == '/pages/about-us')active @endif "><a href="javascript:;">About Us</a></li>
                    <li><a href="{{ route('page-detail','faq') }}">FAQ</a></li>
                    <li><a href="{{ route('page-detail','privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ route('page-detail','terms-and-conditions') }}">Terms and Conditions</a></li>
                </ul>
            </li>
            <li class = " @if(request()->getRequestUri() == '/blog')active @endif "><a href="{{ route('all-blogs') }}">Blog</a></li>
            <li class = " @if(request()->getRequestUri() == '/page/contact-us')active @endif "><a href="{{ route('contact') }}">Contact</a></li>
        </ul>
</nav>
<div id="mobile-menu-wrap"></div>
<div class="header__top__right__social">
    <a href="#"><i class="fab fa-facebook"></i></a>
    <a href="#"><i class="fab fa-twitter"></i></a>
    <a href="#"><i class="fab fa-linkedin"></i></a>
    <a href="#"><i class="fab fa-pinterest-p"></i></a>
</div>
<div class="humberger__menu__contact">
    <ul>
        <li><i class="fa fa-envelope"></i> bhattaganesh05@gmail.com</li>
        <li>Free Shipping for all Order of NPR. 5,000</li>
    </ul>
</div>
</div>
<!-- Humberger End -->
<!-- Header Section Begin -->
<header class="header">
<div class="header__top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="header__top__left">
                    <ul>
                        <li><i class="fa fa-envelope"></i> bhattaganesh05@gmail.com</li>
                        <li>Free Shipping for all Order of NPR. 5,000</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="header__top__right">
                    <div class="header__top__right__social">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-linkedin"></i></a>
                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                    </div>
                    <div class="header__top__right__auth">
                        @guest
                        @if (Route::has('register'))
                        <a href="{{ route('login') }}" title="SignUp"><i class="fa fa-user-plus"></i></a>
                        @endif
                        @if (Route::has('login'))
                        <a href="{{ route('login') }}" title="SignIn"><i class="fa fa-sign-in-alt"></i></a>
                        @endif
                        @else
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form2').submit();">
                                        {{ __('Logout ') }} <i class="fa fa-power-off"></i>
                                    </a>
                                    <form id="logout-form2" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="header__logo">
                <a href="{{ route('landing') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
            </div>
        </div>
        <div class="col-lg-6">
            <nav class="header__menu">
                <ul>
                    <li class=" @if(request()->getRequestUri() == '/')active @endif "><a href="{{ route('landing') }}">Home</a></li>
                    <li class = " @if(request()->getRequestUri() == '/products')active @endif "><a href="{{ route('all-products') }}">Shop</a></li>
                    <li class = " @if(request()->getRequestUri() == '/pages/about-us')active @endif "><a href="{{ route('page-detail','about-us') }}">Pages</a>
                    <ul class="header__menu__dropdown ">
                        <li class = " @if(request()->getRequestUri() == '/pages/about-us')active @endif "><a href="javascript:;">About Us</a></li>
                        <li><a href="{{ route('page-detail','faq') }}">FAQ</a></li>
                        <li><a href="{{ route('page-detail','privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('page-detail','terms-and-conditions') }}">Terms and Conditions</a></li>
                    </ul>
                </li>
                <li class = " @if(request()->getRequestUri() == '/blog')active @endif "><a href="{{ route('all-blogs') }}">Blog</a></li>
                <li class = " @if(request()->getRequestUri() == '/page/contact-us')active @endif "><a href="{{ route('contact') }}">Contact</a></li>
            </ul>
        </nav>
    </div>
    <div class="col-lg-3">
        <div class="header__cart">
            <ul>
                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>0</span></a></li>
            </ul>
            <div class="header__cart__price">item: <span>NPR. {{ number_format(0) }}</span></div>
        </div>
    </div>
</div>
<div class="humberger__open">
    <i class="fa fa-bars"></i>
</div>
</div>
</header>
<!-- Header Section End -->
