@extends('layouts.app')
@section('content')
    <!-- Blog Details Hero Begin -->
    <section class="blog-details-hero set-bg" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog__details__hero__text">
                        <h2 class="text-dark">{{ $page_detail->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Hero End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 order-md-1 order-1">
                    <div class="blog__details__text">
                        <img src="{{ asset('uploads/page/'.$page_detail->image) }}" alt="">
                    </div>
                    <div class="blog__details__content">
                        <div class="row">
                            <div class="col-lg-12">
                                    {!! $page_detail->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->
@endsection
