@extends('layouts.app')
@section('content')
    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Price(in NPR.)</h4>
                            <div class="price-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                     data-min="50" data-max="50000">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                </div>
                                <form action="{{ route('search') }}">
                                    <div class="range-slider">
                                        <div class="price-input">
                                            <input type="text" name = "min" id="minamount" >
                                            <input type="text" name = "max" id="maxamount">
                                            <button type="submit"  class="btn btn-success btn-sm">Filter</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="sidebar__item">
                            <div class="latest-product__text">
                                <h4>Latest Products</h4>
                                <div class="latest-product__slider owl-carousel">
                                    @if($latest_products)
                                        @foreach($latest_products as $key => $product)
                                            @if( $key == 0 || $key == 3)
                                                <div class="latest-prdouct__slider__item">
                                                    @endif
                                                    <a href="{{ route('product-detail',$product->slug) }}" class="latest-product__item">
                                                        <div class="latest-product__item__pic">
                                                            <img src="{{ asset('uploads/product/'.$product->images[0]->image) }}" alt="" style="width: auto;">
                                                        </div>
                                                        <div class="latest-product__item__text">
                                                            <h6>{{ $product->title }}</h6>
                                                            <span>{{"NPR.".number_format($product->actual_cost) }}
                                                                @if($product->discount)
                                                                    <del class="text-danger">{{ $product->price }}</del>
                                                                @endif
                                                </span>
                                                        </div>
                                                    </a>
                                                    @if( $key == 2 || $loop->last)
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <div class="product__discount">
                        <div class="section-title product__discount__title">
                            <h2>Sale Off</h2>
                        </div>
                        <div class="row">
{{--                            <div class="product__discount__slider owl-carousel">--}}
                                @if($product_list->count())
                                    @foreach($product_list as $product)
                                        <div class="col-lg-4">
                                            <div class="product__discount__item">
                                                <div class="product__discount__item__pic set-bg"
                                                     data-setbg="{{ asset('uploads/product/'.$product->images[0]->image) }}">
                                                    <div class="product__discount__percent">
                                                        @if($product->discount > 0) {{"-".$product->discount." %"}} @endif
                                                    </div>
                                                    <ul class="product__item__pic__hover">
                                                        <li><a href="{{ route('product-detail',$product->slug) }}" title = "View Detail"><i class="fa fa-eye"></i></a></li>
                                                        <li><a href="#" title=" Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="product__discount__item__text">
                                                    <h5><a href="#">{{$product->title}}</a></h5>
                                                    <div class="product__item__price">{{ 'NPR. '.number_format($product->actual_cost) }} @if($product->discount > 0) <span>{{"NPR. ".number_format($product->price)}}</span>  @endif </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="product__pagination">
                        {{ $product_list->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Section End -->
@endsection
