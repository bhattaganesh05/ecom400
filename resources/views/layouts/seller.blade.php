@include('seller.partials.header')
<body class="fixed-navbar">
<div class="page-wrapper">
    <!-- START HEADER-->
@include('seller.partials.topbar')
<!-- END HEADER-->
    <!-- START SIDEBAR-->
@include('seller.partials.sidebar')
<!-- END SIDEBAR-->
    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-content fade-in-up">
            @include('seller.partials.notify')
            @yield('main_content')
        </div>
        <!-- END PAGE CONTENT-->
        @include('seller.partials.footer')
    </div>
</div>
<!-- BEGIN THEME CONFIG PANEL-->

<!-- END THEME CONFIG PANEL-->
<!-- BEGIN PAGA BACKDROPS-->
<!-- END PAGA BACKDROPS-->
<!-- CORE PLUGINS-->
