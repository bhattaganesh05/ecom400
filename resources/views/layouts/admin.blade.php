@include('admin.partials.header')
<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('admin.partials.topbar')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
    @include('admin.partials.sidebar')
    <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                @include('admin.partials.notify')
                @yield('main_content')
            </div>
            <!-- END PAGE CONTENT-->
        @include('admin.partials.footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
