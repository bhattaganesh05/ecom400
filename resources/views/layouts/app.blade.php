@include('../front/partials/header')
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    @include('../front/partials/top-header')

       <!-- Hero Section Begin -->
    <section class="hero @if(request()->getRequestUri() != '/') hero-normal @endif">
        <div class="container">
            <div class="row">
            	@include('../front/partials/side-menu')
                @yield('banner')
            </div>
        </div>
    </section>
<!-- Hero Section End -->

@yield('content')
    <!-- Footer Section Begin -->
   @include('../front/partials/footer')
    <!-- Footer Section End -->
    <!-- Js Plugins -->
   @include('../front/partials/script')
