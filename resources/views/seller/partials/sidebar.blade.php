<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                @if(isset(request()->user()->userInfo['image']) && !empty(request()->user()->userInfo['image']))
                    <img src="{{asset('uploads/user/'.request()->user()->userInfo['image'])}}" width="45px" class="img-circle"/>
                @else
                    <img src="{{asset('img/default_user.jpg')}}"  width="45px" />
                @endif
            </div>
            <div class="admin-info">
                <div class="font-strong">{{auth()->user()->name}}</div><small>{{ucfirst(auth()->user()->role)}}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{route(request()->user()->role)}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>

            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-bag"></i>
                    <span class="nav-label">Product Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('seller.product.create') }}">Add Product</a>
                    </li>
                    <li>
                        <a href="{{ route('seller.product') }}">List Product</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-basket"></i>
                    <span class="nav-label">Collection Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('seller.collection.create') }}">Add Collection</a>
                    </li>
                    <li>
                        <a href="{{ route('seller.collection') }}">List Collection</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
                    <span class="nav-label">Order Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Order</a>
                    </li>
                    <li>
                        <a href="cards.html">List Order</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-comments"></i>
                    <span class="nav-label">Review Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="cards.html">Add Review</a>
                    </li>
                    <li>
                        <a href="cards.html">List Review</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
