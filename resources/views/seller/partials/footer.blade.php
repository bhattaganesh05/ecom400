<footer class="page-footer">
    <div class="font-13 m-auto">{{date('Y')}} © <b>Broadway CMS</b> - All rights reserved.</div>
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>
<!-- User update  Modal -->
<div class="modal fade" id="editProfile" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit Profile</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        {{ Form::open(['url'=>route('update-me') ,'class'=>'form','files'=>true]) }}
            @method('patch')
            <!-- Modal body -->
            <div class="modal-body">
               <div class="form-group row">
               {{ Form::label('name','Name',['class'=>'col-sm-12 col-md-3']) }}
                   <div class="col-sm-12 col-md-9">
                       {{ Form::text('name',auth()->user()->name,['class'=>'form-control form-control-sm','id'=>'name','required'=>true,'placeholder'=>'Enter your name']) }}
                   </div>
               </div>
                <div class="form-group row">
                    {{ Form::label('address','Address',['class'=>'col-sm-12 col-md-3']) }}
                    <div class="col-sm-12 col-md-9">
                        {{ Form::text('address',@(auth()->user()->userInfo['address']),['class'=>'form-control form-control-sm','id'=>'address','required'=>false,'placeholder'=>'Enter your address']) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('phone','Phone',['class'=>'col-sm-12 col-md-3']) }}
                    <div class="col-sm-12 col-md-9">
                        {{ Form::tel('phone',@(auth()->user()->userInfo['phone']),['class'=>'form-control form-control-sm','id'=>'phone','required'=>false,'placeholder'=>'Enter your phone number']) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('image','Image',['class'=>'col-sm-12 col-md-3']) }}
                    <div class="col-sm-12 col-md-9">
                        {{ Form::file('image',['id'=>'image','required'=>false]) }}
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" >Save</button>

            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- password update   Modal -->
<div class="modal fade" id="updatePassword" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        {{ Form::open(['url'=>route('update-password') ,'class'=>'form']) }}
        @method('patch')
        <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group row">
                    {{ Form::label('current_password','Current Password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                    <div class="col-sm-12 col-md-7">
                        {{ Form::password('current_password',['class'=>'form-control form-control-sm','id'=>'current_password','required'=>true,'placeholder'=>'Enter your current password']) }}
                    </div>
                </div>

                <div class="form-group row">
                    {{ Form::label('password','New Password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                    <div class="col-sm-12 col-md-7">
                        {{ Form::password('password',['class'=>'form-control form-control-sm','id'=>'password','required'=>true,'placeholder'=>'Enter your new password']) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('confirmation_password','Confirmation Password',['class'=>'col-sm-12 col-md-5 text-md-right']) }}
                    <div class="col-sm-12 col-md-7">
                        {{ Form::password('password_confirmation',['class'=>'form-control form-control-sm','id'=>'confirmation_password','required'=>true,'placeholder'=>'Re-enter your new password ']) }}
                    </div>
                </div>
            <!-- Modal footer -->
            <div class="modal-footer" >
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" >Update</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<script src="{{mix('js/manifest.js')}}" ></script>
<script src="{{mix('js/vendor.js')}}" ></script>
<script src="{{mix('js/admin.js')}}"></script>
@yield('scripts')
</body>

</html>
