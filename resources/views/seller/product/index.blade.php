@extends('layouts.seller')
@section('title','Product List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Product List</div>
                    <a href="{{ route('seller.product.create') }}" title="Add Product" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table ">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Stock</th>
                        <th>Featured</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($product_list->count())
                            @foreach($product_list as $product)
                                <tr>
                                    <td>{{$product->title}}</td>
                                    <td>{{"NPR. ".$product->actual_cost." (".$product->price.")"}}</td>
                                    <td>{{ucfirst($product->category->title) }}
                                        @if($product->sub_category_id != null)
                                            <sub>{{ ucfirst($product->subCategory->title) }}</sub>
                                        @endif
                                    </td>
                                    <td>{{ $product->stock }}</td>
                                    <td>{{ $product->is_featured == 1 ? 'Yes' : 'No' }}</td>
                                    <td>
                                        @if(request()->user()->role == 'admin')
                                        <a href="{{ route('product-change-status',[$product->id,$product->status]) }}" class="badge badge-{{ $product->status == 'active' ? 'success' : 'danger' }}">{{ $product->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                        @else
                                            <a href="javascript:;" class="badge badge-{{ $product->status == 'active' ? 'success' : 'danger' }}">{{ $product->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('seller.product.edit',$product->id) }}" title="Edit this product" class="btn btn-primary btn-sm btn-circle" >
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this product" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('seller.product.destroy',$product->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

