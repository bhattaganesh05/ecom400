@extends('layouts.seller')
@section('title','Collection List || Ecommerce Website')
@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">All Collection List</div>
                    <a href="{{ route('seller.collection.create') }}" title="Add Collection" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></a>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover data-table">
                        <thead class="thead-dark">
                        <th>Title</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($collection_list->count())
                            @foreach($collection_list as $collection)
                                <tr>
                                    <td>{{$collection->title}}</td>
                                    <td>
                                        <a href="{{ asset('uploads/collection/'.$collection->image) }}" data-lightbox = "image-{{ $collection->id }}" data-title = {{ $collection->title }}>Preview</a>
                                    </td>
                                    <td>
                                        @if(request()->user()->role == 'admin')
                                        <a href="{{ route('collection-change-status',[$collection->id,$collection->status]) }}" class="badge badge-{{ $collection->status == 'active' ? 'success' : 'danger' }}">{{ $collection->status == 'active' ? 'Published' : 'Un-published' }}</a>
                                        @else
                                            <a href="javascript:;" class="badge badge-{{ $collection->status == 'active' ? 'success' : 'danger' }}">{{ $collection->status == 'active' ? 'Published' : 'Un-published' }}</a>

                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('seller.collection.edit',$collection->id) }}" title="Edit this collection" class="btn btn-primary btn-sm btn-circle">
                                            <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="javascript:;" title="Delete this collection" class="btn btn-warning btn-sm btn-circle btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        {{ Form::open(['url' => route('seller.collection.destroy',$collection->id) , 'class' => 'delete-form' ]) }}
                                        @method('delete')
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
