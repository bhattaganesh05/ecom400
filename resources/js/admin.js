global.$=global.jQuery=require('jquery');
require('bootstrap/dist/js/bootstrap');
require('metismenu/dist/metisMenu');
require('jquery-slimscroll/jquery.slimscroll');
require('../templates/admin/assets/js/app');
require('lightbox2/dist/js/lightbox');
require('datatables.net-dt/js/dataTables.dataTables');
require('../plugins/summernote/summernote-bs4');
require('select2/dist/js/select2');
$('.edit-profile').click(function (e) {
    e.preventDefault();
    $('#editProfile').modal('show');
});

$('.update-password').click(function (e) {
    e.preventDefault();
    $('#updatePassword').modal('show');
});

setTimeout(() => {
    $('.alert').slideUp();
}, 3000);

$('.data-table').DataTable();
$('#description').summernote({
    height:200
});

$('.btn-delete').click(function (e) {
e.preventDefault();
var confirmed = confirm("Are you sure you want to delete this data?");
if(confirmed){
    $(this).parent().find('form').submit();
}
});

global.readURL= function(input,image_id){
    if(input.files && input.files[0]){
        var reader  = new FileReader();
        reader.onload = function (e){
            $('#'+image_id)
                .attr('src',e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
global.imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {

                    $('div.gallery').append('<div class="col-md-2 mt-1"><a href="'+event.target.result+'" data-lightbox = "image-1"> <img src="'+event.target.result+'" class = "img-fluid img-thumbnail"></a></div>');
                    // $($.parseHTML('<img>')).attr({'src':event.target.result,'class':'img-fluid img-thumbnail','data-lightbox':'image-1','data-title':event.target.name}).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }

    }

global.deleteThisCategory = function (elem) {
    var confirmed = confirm("Are you sure you want to delete this data?");
    if(confirmed){
        $(elem).parent().find('form').submit();
    }
}


