<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page_list = [
            [
                'title' => 'About Us',
                'slug' => \Str::slug('About Us'),
                'status' => 'active',
                'summary' => "this is summary"

            ],
            [
                'title' => 'FAQ',
                'slug' => \Str::slug('FAQ'),
                'status' => 'active',
                'summary' => "this is summary"
            ],
            [
                'title' => 'Privacy Policy',
                'slug' => \Str::slug('Privacy Policy'),
                'status' => 'active',
                'summary' => "this is summary"
            ],
            [
                'title' => 'Terms and Conditions',
                'slug' => \Str::slug('Terms and Conditions'),
                'status' => 'active',
                'summary' => "this is summary"
            ]
        ];
        foreach ($page_list as  $page){
            if(Page::where('slug',$page['slug'])->count() <= 0){
                Page::create($page);
            }
        }
    }
}
