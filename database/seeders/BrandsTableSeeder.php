<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand_list = [
            [
                'title' => 'Apple',
                'slug' => \Str::slug('apple'),
                'status' => 'active',
                'added_by' => 1

            ],
            [
                'title' => 'Levis',
                'slug' => \Str::slug('levis'),
                'status' => 'active',
                'added_by' => 1
            ],
            [
                'title' => 'Zara',
                'slug' => \Str::slug('Zara'),
                'status' => 'active',
                'added_by' => 1
            ],
            [
                'title' => 'Demin',
                'slug' => \Str::slug('Demin'),
                'status' => 'active',
                'added_by' => 1
            ]
        ];
        foreach ($brand_list as  $brand){
            if(Brand::where('slug',$brand['slug'])->count() <= 0){
                Brand::create($brand);
            }
        }
    }
}
