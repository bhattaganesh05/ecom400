<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_user = array(
        	array(
        		'email' => 'admin@gmail.com',
        		'name' => 'Admin User',
        		'password'=> bcrypt('#Admin123'),
        		'role' => 'admin',
        		'status' => 'active'
        	),
        	array(
        		'email' => 'customer@gmail.com',
        		'name' => 'Customer User',
        		'password'=> bcrypt('#Customer123'),
        		'role' => 'customer',
        		'status' => 'active'
        	),
        	array(
        		'email' => 'seller@gmail.com',
        		'name' => 'Seller User',
        		'password'=> bcrypt('#Seller123'),
        		'role' => 'seller',
        		'status' => 'active'
        	)

        );
        foreach ($admin_user as $key => $value) {
        	if(User::where('email',$value['email'])->count() <= 0){
        		User::create($value);
        	}
        }
    }
}
