<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique();
            $table->foreignId('category_id')->nullable()->constrained('categories','id')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreignId('sub_category_id')->nullable()->constrained('categories','id')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->text('summary');
            $table->longText('description')->nullable();
            $table->float('price');
            $table->float('discount')->nullable();
            $table->float('actual_cost');
            $table->integer('stock')->default(0);
            $table->foreignId('seller_id')->nullable()->constrained('users','id')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreignId('brand_id')->nullable()->constrained('brands','id')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->boolean('is_featured')->default(false);
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->foreignId('added_by')->nullable()->constrained('users','id')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
