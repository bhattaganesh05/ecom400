<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();
            $table->foreignId('reviewed_by')->nullable()->constrained('users','id')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreignId('product_id')->constrained('products','id')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->text('review')->nullable();
            $table->integer('rate')->nullable();
            $table->enum('status',['active','inactive','modified'])->default('inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
