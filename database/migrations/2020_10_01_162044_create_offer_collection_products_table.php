<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferCollectionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_collection_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('collection_id')->constrained('offer_product_collections','id')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreignId('product_id')->constrained('products','id')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_collection_products');
    }
}
