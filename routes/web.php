<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.home')->name('landing');
});
Route::group(['namespace'=>'\App\Http\Controllers'],function (){
    Route::get('/', 'FrontendController@homePage')->name('landing');
    Route::get('products','FrontendController@listAllProducts')->name('all-products');
    Route::get('products/{slug}','FrontendController@getProductDetail')->name('product-detail');
    Route::get('collections/{slug}','FrontendController@getCollectionDetail')->name('collection-detail');
    Route::get('pages/{slug}','FrontendController@getPageDetail')->name('page-detail');
    Route::get('blog','FrontendController@getAllBlog')->name('all-blogs');
    Route::get('blog/{slug}','FrontendController@blogDetail')->name('blog-detail');
    Route::get('page/contact-us','FrontendController@getContactUs')->name('contact');
    Route::get('search','FrontendController@searchResult')->name('search');
    Route::get('category','FrontendController@listAllProducts');
    Route::get('category/{slug}','FrontendController@getProductFromCategory')->name('cat-products');
    Route::get('category/{parent_slug}/{slug}','FrontendController@getProductFromSubCategory')->name('sub-cat-products');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix'=> '/admin','middleware'=> ['auth','admin'],'namespace' => '\App\Http\Controllers'], function (){
    Route::get('/','HomeController@admin')->name('admin');
    Route::resource('banner','BannerController')->except('show');
    Route::get('/banner/status-change/{id}/{status}','BannerController@changeStatus')->name('banner-change-status');
    Route::resource('category','CategoryController');
    Route::get('/category/status-change/{id}/{status}','CategoryController@changeStatus')->name('category-change-status');
    Route::resource('brand','BrandController')->except('show');
    Route::get('/brand/status-change/{id}/{status}','BrandController@changeStatus')->name('brand-change-status');
    Route::resource('product','ProductController');
    Route::get('/product/status-change/{id}/{status}','ProductController@changeStatus')->name('product-change-status');
    Route::resource('collection','ProductCollectionController');
    Route::get('/collection/status-change/{id}/{status}','ProductCollectionController@changeStatus')->name('collection-change-status');
    Route::resource('offer','OfferProductCollectionController');
    Route::get('/offer/status-change/{id}/{status}','OfferProductCollectionController@changeStatus')->name('offer-change-status');
    Route::resource('user','UserController');
    Route::get('/user/status-change/{id}/{status}','UserController@changeStatus')->name('user-change-status');
    Route::get('/user/{id}/change-password','UserController@showPasswordChangeForm')->name('change-pwd');
    Route::patch('/user/{id}/change-password','UserController@updatePassword')->name('user-update-password');
    Route::resource('page','PageController')->except(['show','store','delete']);
    Route::get('/page/status-change/{id}/{status}','PageController@changeStatus')->name('page-change-status');
    Route::resource('blog','BlogController');
    Route::get('/blog/status-change/{id}/{status}','BlogController@changeStatus')->name('blog-change-status');
});
Route::group(['prefix'=> '/customer','middleware'=> ['auth','customer'],'namespace' => '\App\Http\Controllers'], function (){
    Route::get('/','HomeController@customer')->name('customer');

});
Route::group(['prefix'=> '/seller','middleware'=> ['auth','seller'],'namespace' => '\App\Http\Controllers'], function (){
    Route::get('/','HomeController@seller')->name('seller');
    Route::get('/product','ProductController@getProductBySeller')->name('seller.product');
    Route::get('/product/create','ProductController@showSellerProductForm')->name('seller.product.create');
    Route::get('/product/{id}','ProductController@getProductEditForm')->name('seller.product.edit');
    Route::post('/product','ProductController@storeProduct')->name('seller.product.store');
    Route::patch('/product/{id}','ProductController@updateProduct')->name('seller.product.update');
    Route::delete('/product/{id}/destroy','ProductController@deleteProduct')->name('seller.product.destroy');
    Route::get('/collection','ProductCollectionController@getCollectionBySeller')->name('seller.collection');
    Route::get('/collection/create','ProductCollectionController@showSellerCollectionForm')->name('seller.collection.create');
    Route::get('/collection/{id}','ProductCollectionController@getCollectionEditForm')->name('seller.collection.edit');
    Route::post('/collection','ProductCollectionController@storeCollection')->name('seller.collection.store');
    Route::patch('/collection/{id}','ProductCollectionController@updateCollection')->name('seller.collection.update');
    Route::delete('/collection/{id}/destroy','ProductCollectionController@deleteCollection')->name('seller.collection.destroy');
});
Route::patch('/update-me',[App\Http\Controllers\HomeController::class,'updateMe'])->name('update-me')->middleware('auth');
Route::post('/category/sub-cat',[App\Http\Controllers\CategoryController::class,'getAllSubCats'])->name('get-sub-cats')->middleware('auth');
Route::patch('/update-password',[App\Http\Controllers\HomeController::class,'updatePassword'])->name('update-password')->middleware('auth');
